
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.3.1] - 2025-02-05

- Bug fixed: overrides the Visibility [#28736]
- Enhancement: customized the label/message at the end of workflow publication [#28746]

## [v2.3.0] - 2024-07-11

**Enhancement**

- In edit mode, is allowed to delete fields according to data type [#27455]

**Bug fixes**

- Edit facility when Catalogue Profiles mixing fields with namespaces and fields without namespaces [#27826]

## [v2.2.2] - 2024-05-14

**Bug fixes**

- Incident Catalogue edit is leading to duplicate fields [#27455]

**Enhancement**

- The Catalogue should read the (Liferay) Highest Role in the VRE (i.e. the Organization) [#27467]

## [v2.2.1] - 2024-03-19

**Enhancement**

- Implemented the Edit facility [#26640]
- Added spinner during resource creation [#25433]
- Implemented the Delete facility [#26793]


## [v2.1.2] - 2023-03-10

**Bug fixes**

- [#24744] Error null after dataset created


## [v2.1.1] - 2022-10-27

**Enhancements**

- [#24038] Moved to GWT 2.9.0

## [v2.1.0] - 2022-06-28

**Enhancements**

- [#23491] The final URL provided to go to the item is the "Catalogue Portlet URL"
- Moved to maven-portal-bom 3.6.4

## [v2.0.1] - 2021-05-24

**Bug fixes**

- [#21470] Fixing: publishing widget uses the orgTitle instead of orgName

**New**

- Moved to maven-portal-bom 3.6.2

## [v2.0.0] - 2021-04-12

**Enhancements**

- [#19764] Porting ckan-metadata-publisher-widget to catalogue-util-library
- [#20680] Ported to SHUB
- [#19568] Unify and extend the tags allowed values 
- [#20828] Revisited title size and format
- [#20868] Redesigned the "Manage Resources" user experience
- [#21068] Add Resources facility: only HTTPS URLs must be allowed
- [#21153] Upgrade the maven-portal-bom to 3.6.1 version

## [v1.6.2] - 2021-02-08

**Bug Fixes**

- [#20446] Catalogue Publishing Widget: field value unexpectedly added in case of optional field 
- [#20663] Fixing Time_Interval placeholder 


## [v1.6.1] - 2020-06-18

**New Features**

- [#18700] Do not skip regex validation on catalogue item when a field is empty (UI side)


## [v1.6.0] - 2019-09-25

- [Feature #11331] Field repeatability: support for catalogue widget

- [Task #12480] Data Catalogue Publishing widget: filter the VREs for the working gateway widget

- [Feature #17577] Modify ckan-metadata-publisher-widget posts to cite people with @username

- [Support #13188] Fixed some logs


## [v1.5.0] - 2019-06-11

- [Feature #13074] Integrated with 'openlayer-basic-widgets'


## [v1.4.1] - 2018-11-20

- [Bug #12914] The form to add/choice the resources to publishing remains frozen


## [v1.4.0] - 2018-09-28

- [Incident #12563] Error when trying to publish the content of a workspace folder into the catalogue


## [v1.3.3] - 2018-03-02

- Minor update


## [v1.3.2] - 2018-02-26

- Minor update due to interface changed


## [v1.3.1] - 2017-06-26

- External Url files are properly managed as resources


## [v1.3.0] - 2017-05-05

- Metadata model v.3 supported


## [v1.2.3] - 2017-04-05

- Some bug fixes


## [v1.2.2] - 2017-02-27

- Some minor terms changed

- added support for ticket #7207


## [v1.2.1] - 2017-02-02

Minor fixes to speed up role retrievals


## [v1.2.0] - 2016-12-11

A post is automatically created by the Product Catalogue user every time someone publishes

The user now can select all files in the hierarchy and publish them

The user can now associate the product to the available groups

Removed ASL session dependency


## [v1.1.0] - 2016-10-11

Creation of a group is now supported

Association of a dataset to a group is now supported

Products are copied to .catalogue area when publishing is performed from workspace


## [v1.0.0] - 2016-06-01

First Release

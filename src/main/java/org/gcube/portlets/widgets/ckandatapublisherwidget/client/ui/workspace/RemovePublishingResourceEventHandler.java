package org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.workspace;

import com.google.gwt.event.shared.EventHandler;


// TODO: Auto-generated Javadoc
/**
 * The Interface RemovePublishingResourceEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Mar 9, 2021
 */
public interface RemovePublishingResourceEventHandler extends EventHandler {

	/**
	 * On remove resource.
	 *
	 * @param removeResourceEvent the remove resource event
	 */
	void onRemoveResource(RemovePublishingResourceEvent removeResourceEvent);
}

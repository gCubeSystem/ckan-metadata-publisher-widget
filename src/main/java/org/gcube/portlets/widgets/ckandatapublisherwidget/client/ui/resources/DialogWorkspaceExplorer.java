package org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.resources;

import java.util.Arrays;
import java.util.List;

import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.WorkspaceItemSelectedEvent;
import org.gcube.portlets.widgets.wsexplorer.client.notification.WorkspaceExplorerSelectNotification.WorskpaceExplorerSelectNotificationListener;
import org.gcube.portlets.widgets.wsexplorer.client.select.WorkspaceExplorerSelectPanel;
import org.gcube.portlets.widgets.wsexplorer.shared.FilterCriteria;
import org.gcube.portlets.widgets.wsexplorer.shared.Item;
import org.gcube.portlets.widgets.wsexplorer.shared.ItemType;

import com.github.gwtbootstrap.client.ui.Button;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;

public class DialogWorkspaceExplorer extends Composite {

	private static DialogBoxWorkspaceUiBinder uiBinder = GWT.create(DialogBoxWorkspaceUiBinder.class);

	interface DialogBoxWorkspaceUiBinder extends UiBinder<Widget, DialogWorkspaceExplorer> {
	}

	@UiField
	Button okButton;

	@UiField
	Button cancelButton;

	@UiField
	FlowPanel panelContainer;

	private DialogBox dialogBox;

	private Item selectedItem;

	private HandlerManager eventBus;

	public DialogWorkspaceExplorer(DialogBox dialog, HandlerManager eventBus) {
		initWidget(uiBinder.createAndBindUi(this));
		this.dialogBox = dialog;
		this.eventBus = eventBus;

		FilterCriteria filterCriteria = null;
		List<ItemType> selectableTypes = Arrays.asList(ItemType.DOCUMENT, ItemType.EXTERNAL_IMAGE,
				ItemType.EXTERNAL_FILE, ItemType.EXTERNAL_PDF_FILE, ItemType.EXTERNAL_URL, ItemType.REPORT_TEMPLATE,
				ItemType.REPORT, ItemType.CSV, ItemType.MOVIE, ItemType.ZIP, ItemType.RAR, ItemType.HTML, ItemType.XML,
				ItemType.TEXT_PLAIN, ItemType.DOCUMENT, ItemType.PRESENTATION, ItemType.SPREADSHEET, ItemType.METADATA,
				ItemType.PDF_DOCUMENT, ItemType.IMAGE_DOCUMENT, ItemType.URL_DOCUMENT, ItemType.GCUBE_ITEM,
				ItemType.TIME_SERIES

		);

		WorkspaceExplorerSelectPanel selectDialog = new WorkspaceExplorerSelectPanel("Select the file...",
				filterCriteria, selectableTypes);
		selectDialog.setWidth("700px");
		selectDialog.setHeight("370px");

		WorskpaceExplorerSelectNotificationListener listener = new WorskpaceExplorerSelectNotificationListener() {

			@Override
			public void onSelectedItem(Item item) {
				GWT.log("onSelectedItem: " + item);
				selectedItem = item;
			}

			@Override
			public void onFailed(Throwable throwable) {
				GWT.log("onFailed..");

			}

			@Override
			public void onAborted() {
				GWT.log("onAborted..");

			}

			@Override
			public void onNotValidSelection() {
				GWT.log("onNotValidSelection..");

			}
		};

		selectDialog.addWorkspaceExplorerSelectNotificationListener(listener);

		panelContainer.add(selectDialog);
	}

	@UiHandler("okButton")
	void onOKClick(ClickEvent e) {

		if(selectedItem!=null) {
			eventBus.fireEvent(new WorkspaceItemSelectedEvent(selectedItem));
			dialogBox.hide();
		}
	}

	@UiHandler("cancelButton")
	void onCancelClick(ClickEvent e) {
		dialogBox.hide();
	}

}

package org.gcube.portlets.widgets.ckandatapublisherwidget.shared;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * This bean will contain during ckan metadata creation information related to
 * the future build.
 * 
 * revisited by @author Francesco Mangiacrapa at ISTI-CNR
 * 
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 * 
 */
@SuppressWarnings("serial")
public class DatasetBean implements Serializable, IsSerializable {

	private String id;
	private String ckanName;

	public String getCkanName() {
		return ckanName;
	}

	public void setCkanName(String ckanName) {
		this.ckanName = ckanName;
	}

	private String title;
	private String description;
	private String license; // chosen by the user
	private String source; // url of the folder in the workspace
	private String authorName; // author name
	private String authorSurname; // author surname
	private String authorFullName;
	private String authorEmail; // owner's email
	private String maintainer;
	private String maintainerEmail;
	private String ownerIdentifier; // owner of the folder into the workspace (e.g., andrea.rossi)
	private String chosenType; // the name of the MetaDataType chosen
	private String selectedOrganization;
	private long version; // version 1, 2 ...
	private boolean visible; // Private (false) or Public(true)
	private List<OrganizationBean> organizationList; // list of organization in which the user is present and could
														// create the dataset
	private ResourceElementBean resourceRoot; // in case of workspace, this is the directory root or the single file
												// information
	private List<MetaDataProfileBean> metadataList;
	private List<String> tags; // on retrieve, they are the keys of the product
	private List<String> tagsVocabulary; // when available
	private Map<String, List<String>> customFields;
	private List<OrganizationBean> groups;
	private List<OrganizationBean> groupsForceCreation;
	private List<ResourceElementBean> resources;
	private String wsFolderId;

	/**
	 * Instantiates a new dataset bean.
	 */
	public DatasetBean() {
		super();
	}

	/**
	 * Create a metadata bean object.
	 *
	 * @param id                   the id
	 * @param title                the title
	 * @param description          the description
	 * @param customFields         the custom fields
	 * @param tags                 the tags
	 * @param license              the license
	 * @param visible              the visible
	 * @param source               the source
	 * @param version              the version
	 * @param authorName           the author name
	 * @param authorSurname        the author surname
	 * @param authorEmail          the author email
	 * @param maintainer           the maintainer
	 * @param maintainerEmail      the maintainer email
	 * @param ownerIdentifier      the owner identifier
	 * @param organizationList     the organization list
	 * @param selectedOrganization the selected organization
	 * @param resourceRoot         the resource root
	 * @param metadataList         the metadata list
	 * @param groups               the groups
	 * @param tagsVocabulary       the tags vocabulary
	 */
	public DatasetBean(String id, String title, String description, Map<String, List<String>> customFields,
			List<String> tags, String license, boolean visible, String source, long version, String authorName,
			String authorSurname, String authorEmail, String maintainer, String maintainerEmail, String ownerIdentifier,
			List<OrganizationBean> organizationList, String selectedOrganization, ResourceElementBean resourceRoot,
			List<MetaDataProfileBean> metadataList, List<OrganizationBean> groups, List<String> tagsVocabulary) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.customFields = customFields;
		this.tags = tags;
		this.license = license;
		this.visible = visible;
		this.source = source;
		this.version = version;
		this.authorName = authorName;
		this.authorSurname = authorSurname;
		this.authorEmail = authorEmail;
		this.maintainer = maintainer;
		this.maintainerEmail = maintainerEmail;
		this.ownerIdentifier = ownerIdentifier;
		this.organizationList = organizationList;
		this.selectedOrganization = selectedOrganization;
		this.resourceRoot = resourceRoot;
		this.metadataList = metadataList;
		this.groups = groups;
		this.tagsVocabulary = tagsVocabulary;
	}

	/**
	 * Gets the chosen type.
	 *
	 * @return the chosen type
	 */
	public String getChosenType() {
		return chosenType;
	}

	/**
	 * Sets the chosen type.
	 *
	 * @param chosenType the new chosen type
	 */
	public void setChosenType(String chosenType) {
		this.chosenType = chosenType;
	}

	/**
	 * Gets the metadata list.
	 *
	 * @return the metadata list
	 */
	public List<MetaDataProfileBean> getMetadataList() {
		return metadataList;
	}

	/**
	 * Sets the metadata list.
	 *
	 * @param metadataListTypes the new metadata list
	 */
	public void setMetadataList(List<MetaDataProfileBean> metadataListTypes) {
		this.metadataList = metadataListTypes;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the owner identifier.
	 *
	 * @return the owner identifier
	 */
	public String getOwnerIdentifier() {
		return ownerIdentifier;
	}

	/**
	 * Sets the owner identifier.
	 *
	 * @param ownerIdentifier the new owner identifier
	 */
	public void setOwnerIdentifier(String ownerIdentifier) {
		this.ownerIdentifier = ownerIdentifier;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the custom fields.
	 *
	 * @return the custom fields
	 */
	public Map<String, List<String>> getCustomFields() {
		return customFields;
	}

	/**
	 * Sets the custom fields.
	 *
	 * @param customFields the custom fields
	 */
	public void setCustomFields(Map<String, List<String>> customFields) {
		this.customFields = customFields;
	}

	/**
	 * Gets the tags.
	 *
	 * @return the tags
	 */
	public List<String> getTags() {
		return tags;
	}

	/**
	 * Sets the tags.
	 *
	 * @param tags the new tags
	 */
	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	/**
	 * Gets the license.
	 *
	 * @return the license
	 */
	public String getLicense() {
		return license;
	}

	/**
	 * Sets the license.
	 *
	 * @param license the new license
	 */
	public void setLicense(String license) {
		this.license = license;
	}

	/**
	 * Gets the visibility.
	 *
	 * @return the visibility
	 */
	public boolean getVisibility() {
		return visible;
	}

	/**
	 * Sets the visibile.
	 *
	 * @param visibile the new visibile
	 */
	public void setVisibile(boolean visibile) {
		this.visible = visibile;
	}

	/**
	 * Gets the source.
	 *
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * Sets the source.
	 *
	 * @param source the new source
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version the new version
	 */
	public void setVersion(long version) {
		this.version = version;
	}

	/**
	 * Gets the author name.
	 *
	 * @return the author name
	 */
	public String getAuthorName() {
		return authorName;
	}

	/**
	 * Sets the author name.
	 *
	 * @param authorName the new author name
	 */
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	/**
	 * Gets the author surname.
	 *
	 * @return the author surname
	 */
	public String getAuthorSurname() {
		return authorSurname;
	}

	/**
	 * Sets the author surname.
	 *
	 * @param authorSurname the new author surname
	 */
	public void setAuthorSurname(String authorSurname) {
		this.authorSurname = authorSurname;
	}

	/**
	 * Gets the author email.
	 *
	 * @return the author email
	 */
	public String getAuthorEmail() {
		return authorEmail;
	}

	/**
	 * Sets the author email.
	 *
	 * @param authorEmail the new author email
	 */
	public void setAuthorEmail(String authorEmail) {
		this.authorEmail = authorEmail;
	}

	/**
	 * Gets the maintainer.
	 *
	 * @return the maintainer
	 */
	public String getMaintainer() {
		return maintainer;
	}

	/**
	 * Sets the maintainer.
	 *
	 * @param maintainer the new maintainer
	 */
	public void setMaintainer(String maintainer) {
		this.maintainer = maintainer;
	}

	/**
	 * Gets the maintainer email.
	 *
	 * @return the maintainer email
	 */
	public String getMaintainerEmail() {
		return maintainerEmail;
	}

	/**
	 * Sets the maintainer email.
	 *
	 * @param maintainerEmail the new maintainer email
	 */
	public void setMaintainerEmail(String maintainerEmail) {
		this.maintainerEmail = maintainerEmail;
	}

	/**
	 * Gets the organization list.
	 *
	 * @return the organization list
	 */
	public List<OrganizationBean> getOrganizationList() {
		return organizationList;
	}

	/**
	 * Sets the organization list.
	 *
	 * @param organizationList the new organization list
	 */
	public void setOrganizationList(List<OrganizationBean> organizationList) {
		this.organizationList = organizationList;
	}

	/**
	 * Gets the selected organization.
	 *
	 * @return the selected organization
	 */
	public String getSelectedOrganization() {
		return selectedOrganization;
	}

	/**
	 * Sets the selected organization.
	 *
	 * @param selectedOrganization the new selected organization
	 */
	public void setSelectedOrganization(String selectedOrganization) {
		this.selectedOrganization = selectedOrganization;
	}

	/**
	 * Gets the resource root.
	 *
	 * @return the resource root
	 */
	public ResourceElementBean getResourceRoot() {
		return resourceRoot;
	}

	/**
	 * Sets the resource root.
	 *
	 * @param resourceRoot the new resource root
	 */
	public void setResourceRoot(ResourceElementBean resourceRoot) {
		this.resourceRoot = resourceRoot;
	}

	/**
	 * Gets the author full name.
	 *
	 * @return the author full name
	 */
	public String getAuthorFullName() {
		return authorFullName;
	}

	/**
	 * Sets the author full name.
	 *
	 * @param authorFullName the new author full name
	 */
	public void setAuthorFullName(String authorFullName) {
		this.authorFullName = authorFullName;
	}

	/**
	 * Gets the groups.
	 *
	 * @return the groups
	 */
	public List<OrganizationBean> getGroups() {
		return groups;
	}

	/**
	 * Sets the groups.
	 *
	 * @param groups the new groups
	 */
	public void setGroups(List<OrganizationBean> groups) {
		this.groups = groups;
	}

	/**
	 * Gets the tags vocabulary.
	 *
	 * @return the tags vocabulary
	 */
	public List<String> getTagsVocabulary() {
		return tagsVocabulary;
	}

	/**
	 * Sets the tags vocabulary.
	 *
	 * @param tagsVocabulary the new tags vocabulary
	 */
	public void setTagsVocabulary(List<String> tagsVocabulary) {
		this.tagsVocabulary = tagsVocabulary;
	}

	/**
	 * Gets the groups force creation.
	 *
	 * @return the groups force creation
	 */
	public List<OrganizationBean> getGroupsForceCreation() {
		return groupsForceCreation;
	}

	/**
	 * Sets the groups force creation.
	 *
	 * @param groupsForceCreation the new groups force creation
	 */
	public void setGroupsForceCreation(List<OrganizationBean> groupsForceCreation) {
		this.groupsForceCreation = groupsForceCreation;
	}

	/**
	 * Sets the resources.
	 *
	 * @param resources the new resources
	 */
	public void setResources(List<ResourceElementBean> resources) {
		this.resources = resources;
	}

	/**
	 * Gets the ckan resources.
	 *
	 * @return the ckan resources
	 */
	public List<ResourceElementBean> getCkanResources() {
		return resources;
	}

	public void setWSFolderId(String folderId) {
		this.wsFolderId = folderId;

	}

	public String getWsFolderId() {
		return wsFolderId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DatasetBean [id=");
		builder.append(id);
		builder.append(", ckanName=");
		builder.append(ckanName);
		builder.append(", title=");
		builder.append(title);
		builder.append(", description=");
		builder.append(description);
		builder.append(", license=");
		builder.append(license);
		builder.append(", source=");
		builder.append(source);
		builder.append(", authorName=");
		builder.append(authorName);
		builder.append(", authorSurname=");
		builder.append(authorSurname);
		builder.append(", authorFullName=");
		builder.append(authorFullName);
		builder.append(", authorEmail=");
		builder.append(authorEmail);
		builder.append(", maintainer=");
		builder.append(maintainer);
		builder.append(", maintainerEmail=");
		builder.append(maintainerEmail);
		builder.append(", ownerIdentifier=");
		builder.append(ownerIdentifier);
		builder.append(", chosenType=");
		builder.append(chosenType);
		builder.append(", selectedOrganization=");
		builder.append(selectedOrganization);
		builder.append(", version=");
		builder.append(version);
		builder.append(", visible=");
		builder.append(visible);
		builder.append(", organizationList=");
		builder.append(organizationList);
		builder.append(", resourceRoot=");
		builder.append(resourceRoot);
		builder.append(", metadataList=");
		builder.append(metadataList);
		builder.append(", tags=");
		builder.append(tags);
		builder.append(", tagsVocabulary=");
		builder.append(tagsVocabulary);
		builder.append(", customFields=");
		builder.append(customFields);
		builder.append(", groups=");
		builder.append(groups);
		builder.append(", groupsForceCreation=");
		builder.append(groupsForceCreation);
		builder.append(", wsFolderId=");
		builder.append(wsFolderId);
		builder.append("]");
		return builder.toString();
	}

}
package org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.resources;

import org.gcube.portlets.widgets.ckandatapublisherwidget.client.CKanPublisherService;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.CKanPublisherServiceAsync;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.AddResourceEvent;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.ReloadDatasetPageEvent;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.WorkspaceItemSelectedEvent;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.WorkspaceItemSelectedEventHandler;
import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.ResourceElementBean;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.utils.LoaderIcon;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.AlertBlock;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.ControlGroup;
import com.github.gwtbootstrap.client.ui.TextArea;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.ControlGroupType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class AddResourceToDataset.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 23, 2024
 */
public class AddResourceToDataset extends Composite {

	private static AddResourceToDatasetUiBinder uiBinder = GWT.create(AddResourceToDatasetUiBinder.class);

	/**
	 * The Interface AddResourceToDatasetUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Feb 23, 2024
	 */
	interface AddResourceToDatasetUiBinder extends UiBinder<Widget, AddResourceToDataset> {
	}

	// bus to alert the dataset form about this new resource
	private HandlerManager eventBus;

	// the dataset id
	private String datasetId;

	// the dataset organization
	private String datasetOrg;

	private final CKanPublisherServiceAsync ckanServices = GWT.create(CKanPublisherService.class);

	@UiField
	TextBox resourceUrlTextBox;
	@UiField
	TextBox resourceNameTextBox;
	@UiField
	TextArea resourceDescriptionTextArea;
	@UiField
	Button addResourceButton;
	@UiField
	AlertBlock infoBlock;
	@UiField
	Button goToDatasetButton;
	@UiField
	ControlGroup urlControlGroup;
	@UiField
	ControlGroup nameControlGroup;
	@UiField
	FlowPanel infoPanel;
	@UiField
	Button buttoSelectFromWorkspace;
	@UiField
	Alert alertInfoURL;

	FlowPanel alertPanel = new FlowPanel();

	/**
	 * Instantiates a new adds the resource to dataset.
	 *
	 * @param eventBus     the event bus
	 * @param datasetId    the dataset id
	 * @param datasetTitle the dataset title
	 * @param datasetOrg   the dataset org
	 * @param datasetUrl   the dataset url
	 */
	public AddResourceToDataset(HandlerManager eventBus, final String datasetId, String datasetTitle,
			final String datasetOrg, final String datasetUrl) {
		initWidget(uiBinder.createAndBindUi(this));
		// save bus
		this.eventBus = eventBus;
		// save dataset id (it is needed when we will add resources)
		this.datasetId = datasetId;
		this.datasetOrg = datasetOrg;

		String title = datasetTitle;
		String link = "";
		link += title.length() > 90 ? title.substring(0, 90) + "..." : title;
		goToDatasetButton.setTitle("Go to the item: " + title);
		goToDatasetButton.setText(link);
		// goToDatasetButton.setHref(datasetUrl);
		goToDatasetButton.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				Window.open(datasetUrl, "_blank", "");
			}
		});

		alertInfoURL.setType(AlertType.WARNING);

		infoBlock.add(alertPanel);

		bind();

		addResourceButton.setEnabled(true);

	}

	/**
	 * Bind.
	 */
	private void bind() {

		eventBus.addHandler(WorkspaceItemSelectedEvent.TYPE, new WorkspaceItemSelectedEventHandler() {

			@Override
			public void onSelectedItem(final WorkspaceItemSelectedEvent workspaceItemSelectedEvent) {

				if (workspaceItemSelectedEvent.getItem() != null) {
					LoaderIcon loader = new LoaderIcon("Getting public link...");
					showAlert(null, loader, AlertType.INFO, false, true);

					ckanServices.getPublicLinkForFileItemId(workspaceItemSelectedEvent.getItem().getId(), true,
							new AsyncCallback<String>() {

								@Override
								public void onFailure(Throwable caught) {
									showAlert(caught.getMessage(), null, AlertType.ERROR, false, true);

								}

								@Override
								public void onSuccess(String result) {
									showAlert("", null, null, false, false);
									resourceUrlTextBox.setText(result);
									resourceNameTextBox.setText(workspaceItemSelectedEvent.getItem().getName());
								}
							});
				}

			}
		});

	}

	/**
	 * On select from workspace click.
	 *
	 * @param e the e
	 */
	@UiHandler("buttoSelectFromWorkspace")
	void onSelectFromWorkspaceClick(ClickEvent e) {

		final DialogBox dialog = new DialogBox(false);
		dialog.setText("Select file from Workspace...");
		dialog.getElement().setId("selectWorkspaceDialog");

		dialog.getElement().getStyle().setWidth(700, Unit.PX);
		dialog.getElement().getStyle().setZIndex(10000);
		dialog.setHeight("400px");

		DialogWorkspaceExplorer dbw = new DialogWorkspaceExplorer(dialog, eventBus);
		dialog.add(dbw);
		dialog.center();
	}

	/**
	 * On add button click.
	 *
	 * @param e the e
	 */
	@UiHandler("addResourceButton")
	void onAddButtonClick(ClickEvent e) {

		infoBlock.setVisible(false);
		urlControlGroup.setType(ControlGroupType.NONE);
		nameControlGroup.setType(ControlGroupType.NONE);

		// validation
		if (resourceUrlTextBox.getText().isEmpty()) {

			showAlert("'URL' field cannot be empty", null, AlertType.ERROR, true, true);
			urlControlGroup.setType(ControlGroupType.ERROR);
			return;
		}

		// validation
		if (resourceNameTextBox.getText().isEmpty() || resourceNameTextBox.getText().isEmpty()) {

			showAlert("'Name' field cannot be empty", null, AlertType.ERROR, true, true);
			nameControlGroup.setType(ControlGroupType.ERROR);
			return;
		}

		// THE URL must be HTTPS, see #21068
		if (!(resourceUrlTextBox.getText().toLowerCase().startsWith("https://"))) {
			showAlert("The URL must be HTTPS, so start with \"https://\" (e.g. https://your-resource.com)", null,
					AlertType.ERROR, true, true);
			urlControlGroup.setType(ControlGroupType.ERROR);
			return;

		}

		// collect data and build up the bean
		final ResourceElementBean resource = new ResourceElementBean(resourceNameTextBox.getText(), true, false, null,
				null, null, null, null, resourceUrlTextBox.getText(), resourceDescriptionTextArea.getText(),
				datasetOrg);

		// disable add button
		addResourceButton.setEnabled(false);

		LoaderIcon loader = new LoaderIcon("Adding resource, please wait...");
		infoPanel.add(loader);

		// try to create
		ckanServices.addResourceToDataset(resource, datasetOrg, datasetId, new AsyncCallback<ResourceElementBean>() {

			@Override
			public void onSuccess(ResourceElementBean result) {
				infoPanel.clear();

				if (result != null) {
					showAlert("Resource added correctly", null, AlertType.SUCCESS, true, true);
					eventBus.fireEvent(new AddResourceEvent(result));
					eventBus.fireEvent(new ReloadDatasetPageEvent(datasetId));

					// remove data
					resourceUrlTextBox.setText("");
					resourceNameTextBox.setText("");
					resourceDescriptionTextArea.setText("");

				} else
					showAlert("Unable to add this resource. Check that the url is correct", null, AlertType.ERROR, true,
							true);

				addResourceButton.setEnabled(true);

			}

			@Override
			public void onFailure(Throwable caught) {
				infoPanel.clear();
				showAlert("Unable to add this resource, sorry. Error is: " + caught.getMessage(), null, AlertType.ERROR,
						true, true);

				addResourceButton.setEnabled(true);

			}
		});

	}

	/**
	 * Show error/success after resource creation attempt.
	 *
	 * @param text         the text
	 * @param loader       the loader
	 * @param type         the type
	 * @param scheduleHide the schedule hide
	 * @param setVisible   the set visible
	 */
	protected void showAlert(String text, LoaderIcon loader, AlertType type, boolean scheduleHide, boolean setVisible) {

		alertPanel.clear();

		infoBlock.setType(type);
		infoBlock.setVisible(setVisible);

		if (scheduleHide) {
			// hide after some seconds
			Timer t = new Timer() {

				@Override
				public void run() {

					infoBlock.setVisible(false);

				}
			};

			t.schedule(8000);
		}

		if (text != null) {
			alertPanel.add(new HTML(text));
		}

		if (loader != null) {
			alertPanel.add(loader);
		}

	}
}

package org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.form;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.gcube.portlets.widgets.ckandatapublisherwidget.client.CKanMetadataPublisher;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.CKanPublisherServiceAsync;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.AddResourceEvent;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.CloseCreationFormEvent;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.CloseCreationFormEventHandler;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.DeleteCustomFieldEvent;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.DeleteCustomFieldEventHandler;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.ReloadDatasetPageEvent;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.ReloadDatasetPageEventHandler;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.metadata.CustomFieldEntry;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.resources.AddResourceToDataset;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.resources.ManageResources;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.utils.InfoIconsLabels;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.utils.WizardCreator;
import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.DatasetBean;
import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.MetadataProfileBeanForUpdate;
import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.OrganizationBean;
import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.ResourceElementBean;
import org.gcube.portlets.widgets.mpformbuilder.client.form.MetaDataField;
import org.gcube.portlets.widgets.mpformbuilder.client.form.generic.CreateMetadataForm.OPERATION;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.metadata.CategoryPanel;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.metadata.MetaDataFieldSkeleton;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.tags.TagsPanel;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.utils.LoaderIcon;
import org.gcube.portlets.widgets.mpformbuilder.shared.license.LicenseBean;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.CategoryWrapper;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetadataFieldWrapper;

import com.github.gwtbootstrap.client.ui.AlertBlock;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.ControlGroup;
import com.github.gwtbootstrap.client.ui.Form;
import com.github.gwtbootstrap.client.ui.Icon;
import com.github.gwtbootstrap.client.ui.ListBox;
import com.github.gwtbootstrap.client.ui.NavPills;
import com.github.gwtbootstrap.client.ui.Paragraph;
import com.github.gwtbootstrap.client.ui.Popover;
import com.github.gwtbootstrap.client.ui.TextArea;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.ControlGroupType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.github.gwtbootstrap.client.ui.constants.ResizeType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.SelectElement;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Create metadata form for ckan product.
 * 
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public class UpdateDatasetForm extends Composite {

	private static final String VISIBILITY_RESTRICTED_LABEL = "Restricted";
	private static final String VISIBILITY_PUBLIC_LABEL = "Public";
	private static EditMetadataFormUiBinder uiBinder = GWT.create(EditMetadataFormUiBinder.class);

	/**
	 * The Interface EditMetadataFormUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Feb 6, 2024
	 */
	interface EditMetadataFormUiBinder extends UiBinder<Widget, UpdateDatasetForm> {
	}

	@UiField
	HTMLPanel createDatasetMainPanel;
	@UiField
	TextBox titleTextBox;
	@UiField
	TextArea descriptionTextarea;
	@UiField
	ListBox licenseListbox;
	@UiField
	ListBox visibilityListbox;
	@UiField
	ListBox organizationsListbox;
	@UiField
	TextBox versionTextbox;
	@UiField
	TextBox authorTextbox;
	@UiField
	TextBox authorEmailTextbox;
	@UiField
	TextBox maintainerTextbox;
	@UiField
	TextBox maintainerEmailTextbox;
	@UiField
	ControlGroup customFields;
	@UiField
	Button addCustomFieldButton;
	@UiField
	Button updateButton;
	@UiField
	Button resetButton;
	@UiField
	AlertBlock infoBlock;
	@UiField
	AlertBlock alertNoResources;
	@UiField
	AlertBlock onContinueAlertBlock;
	@UiField
	AlertBlock onCreateAlertBlock;
	@UiField
	VerticalPanel metadataFieldsPanel;
	@UiField
	ListBox metadataTypeListbox;
	@UiField
	Form formFirstStep;
	@UiField
	Form formSecondStep;
	@UiField
	Form formThirdStep;
	@UiField
	Button continueButton;
	@UiField
	Button goBackButtonSecondStep;
	@UiField
	Paragraph selectedProfile;
	@UiField
	Button goToDatasetButton;
	@UiField
	HorizontalPanel goToDatasetButtonPanel;
	@UiField
	Button addResourcesButtonStep1;

	@UiField
	Button addResourcesButtonStep3;

	@UiField
	SimplePanel workspaceResourcesContainer;
	@UiField
	Button continueThirdStep;
	@UiField
	Button goBackButtonFirstStep;
	@UiField
	Anchor licenseUrlAnchor;
	@UiField
	Paragraph unavailableUrl;
	@UiField
	TagsPanel tagsPanel;
	@UiField
	ListBox groupsListbox;

	// info panels
	@UiField
	Icon infoIconLicenses;
	@UiField
	FocusPanel focusPanelLicenses;
	@UiField
	Popover popoverLicenses;
	@UiField
	Icon infoIconVisibility;
	@UiField
	FocusPanel focusPanelVisibility;
	@UiField
	Popover popoverVisibility;
	@UiField
	Icon infoIconAuthor;
	@UiField
	FocusPanel focusPanelAuthor;
	@UiField
	Popover popoverAuthor;
	@UiField
	Icon infoIconMaintainerEmail;
	@UiField
	FocusPanel focusPanelMaintainerEmail;
	@UiField
	Popover popoverMaintainerEmail;
	@UiField
	Icon infoIconAuthorEmail;
	@UiField
	FocusPanel focusPanelAuthorEmail;
	@UiField
	Popover popoverAuthorEmail;
	@UiField
	Icon infoIconTypes;
	@UiField
	FocusPanel focusPanelTypes;
	@UiField
	Popover popoverTypes;
	@UiField
	Icon infoIconMaintainer;
	@UiField
	FocusPanel focusPanelMaintainer;
	@UiField
	Popover popoverMaintainer;
	@UiField
	Icon infoIconCustomFields;
	@UiField
	FocusPanel focusPanelCustomFields;
	@UiField
	Popover popoverCustomFields;
	@UiField
	Icon infoIconResources;
	@UiField
	FocusPanel focusPanelResources;
	@UiField
	Popover popoverResources;
	@UiField
	Icon infoIconTitle;
	@UiField
	FocusPanel focusPanelTitle;
	@UiField
	Popover popoverGroups;
	@UiField
	Icon infoIconGroups;
	@UiField
	FocusPanel focusPanelGroups;
	@UiField
	Popover popoverTitle;
	@UiField
	Icon infoIconDescription;
	@UiField
	Popover popoverDescription;
	@UiField
	FocusPanel focusPanelDescription;
	@UiField
	ControlGroup metadataTypesControlGroup;
	@UiField
	ControlGroup productTitleGroup;
	@UiField
	ControlGroup maintainerControlGroup;
	@UiField
	ControlGroup versionControlGroup;
	@UiField
	ControlGroup organizationsGroup;
	@UiField
	ControlGroup groupsControlGroup;

	@UiField
	HTMLPanel wizardCreatorPanel;

	private Icon iconSpinner = new Icon(IconType.SPINNER);

	// Create a remote service proxy to talk to the server-side ckan service.
	private final CKanPublisherServiceAsync ckanServices = CKanPublisherServiceAsync.Util.getInstance();

	// private static final String REGEX_TITLE_PRODUCT_SUBWORD = "[^a-zA-Z0-9_.-]";
	private static final String REGEX_MAIL = "\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b";
	private static final String NONE_PROFILE = "none";

	// error/info messages
	protected static final String ERROR_PRODUCT_CREATION = "There was an error while trying to update the item.";
	protected static final String PRODUCT_UPDATED_OK = "Item correctly updated!";
	protected static final String PRODUCT_UPDATED_SUBMITTED_FOR_REVIEW_OK = "Item correctly updated and submitted for review!";
	private static final String TRYING_TO_UPDATE_PRODUCT = "Trying to update the item, please wait...";
	protected static final String MISSING_PUBLISH_RIGHTS = "It seems you are not authorized to edit this item. Either you must be the author item or Catalogue Admininistrator in the organization";

	// tab panel
	private NavPills tabPanel;

	// add resource form
	private AddResourceToDataset resourceForm;

	// the licenses
	private List<LicenseBean> licenseBean;

	// event bus of the principal application (it is the Catalogue Portlet)
	private HandlerManager eventBusMainApplication;

	private HandlerManager eventBusPublisherWidget = new HandlerManager(null);

	// added custom field entries (by the user)
	private List<CustomFieldEntry> customFieldEntriesList = new ArrayList<CustomFieldEntry>();

	// the list of MetaDataField added
	private List<MetaDataField> listOfMetadataFields = new ArrayList<MetaDataField>();

	// dataset metadata bean
	private DatasetBean theDatasetBean;

	// resource table
	// private SelectResourceByWEMainPanel resourcesSelectByWEMainPanel;

	// List of opened popup'ids
	private List<String> popupOpenedIds = new ArrayList<String>();

	// map of organization name title
	private Map<String, String> nameTitleOrganizationMap = new HashMap<String, String>();

	private WizardCreator wizCreator;

	private boolean isWorkspaceRequest = false;

	private String datasetIdOrName = null;

	private FlowPanel alertPanel = new FlowPanel();

	// Feature #28746
	private Boolean catalogueModerated = false;

	/**
	 * Instantiates a new update dataset form.
	 *
	 * @param eventBusMainApplication the event bus
	 * @param itemId                  the item id
	 */
	public UpdateDatasetForm(HandlerManager mainApplicationBusRef, String itemId) {
		updateDatasetFormBody(mainApplicationBusRef, itemId, OPERATION.UPDATE);
	}

	/**
	 * Bind on events.
	 */
	private void bind() {
		// when a custom field is removed, remove it from the list
		eventBusPublisherWidget.addHandler(DeleteCustomFieldEvent.TYPE, new DeleteCustomFieldEventHandler() {

			@Override
			public void onRemoveEntry(DeleteCustomFieldEvent event) {
				customFieldEntriesList.remove(event.getRemovedEntry());
				customFields.remove(event.getRemovedEntry());
			}
		});

		// on close form
		eventBusPublisherWidget.addHandler(CloseCreationFormEvent.TYPE, new CloseCreationFormEventHandler() {
			@Override
			public void onClose(CloseCreationFormEvent event) {
				InfoIconsLabels.closeDialogBox(popupOpenedIds);
			}
		});

		eventBusPublisherWidget.addHandler(ReloadDatasetPageEvent.TYPE, new ReloadDatasetPageEventHandler() {

			@Override
			public void onReloadDatasetPage(ReloadDatasetPageEvent addResourceEvent) {

				// Firing the event on the main application bus
				if (addResourceEvent.getDatasetIDorName() != null) {
					eventBusMainApplication
							.fireEvent(new ReloadDatasetPageEvent(addResourceEvent.getDatasetIDorName()));
				}

			}
		});
	}

	/**
	 * The real constructor.
	 *
	 * @param eventBusMainApplication the event bus
	 * @param datasetIdOrName         the item id
	 * @param operation               the operation
	 */
	private void updateDatasetFormBody(final HandlerManager mainApplicationBusRef, final String datasetIdOrName,
			final OPERATION operation) {

		initWidget(uiBinder.createAndBindUi(this));

		this.eventBusMainApplication = mainApplicationBusRef;
		this.datasetIdOrName = datasetIdOrName;

		iconSpinner.setSpin(true);
		iconSpinner.getElement().getStyle().setMarginLeft(5, Unit.PX);
		iconSpinner.getElement().getStyle().setProperty("animation", "spin 1s infinite linear");

		infoBlock.add(alertPanel);

		List<String> listOfSteps = Arrays.asList("Edit Common Metadata", "Edit Item Specific Metadata & Update",
				"Manage Resources");

		this.wizCreator = new WizardCreator(listOfSteps);
		wizardCreatorPanel.add(wizCreator);

		this.descriptionTextarea.setResize(ResizeType.VERTICAL);
		bind();
		prepareInfoIcons();

		// disable continue button
		continueButton.setEnabled(false);
		resetButton.setEnabled(false);
		addResourcesButtonStep1.setEnabled(false);

		// hide reset button
		resetButton.setVisible(false);

		// hide tags panel
		tagsPanel.setVisible(false);

		// check if the user has publishing rights
		LoaderIcon loader = new LoaderIcon("Checking your permissions, please wait...");
		setAlertBlock(null, loader, AlertType.INFO, true);

		ckanServices.isPublisherOwnerOrAdminUser(this.datasetIdOrName, new AsyncCallback<Boolean>() {

			@Override
			public void onSuccess(Boolean result) {
				GWT.log("isPublisherOwnerOrAdminUser: " + result);
				if (result) {

					// set info block

					LoaderIcon loader = new LoaderIcon("Retrieving information, please wait...");
					setAlertBlock(null, loader, AlertType.INFO, true);

					// get back the licenses and the metadata information
					ckanServices.getDatasetBeanForUpdate(datasetIdOrName, new AsyncCallback<DatasetBean>() {

						@Override
						public void onFailure(Throwable caught) {

							setAlertBlock(caught.getMessage(), null, AlertType.ERROR, true);

						}

						@Override
						public void onSuccess(final DatasetBean datasetBean) {
							GWT.log("getDatasetBeanForUpdate returns: " + datasetBean);

							if (datasetBean == null) {
								setAlertBlock("Error while retrieving information.", null, AlertType.ERROR, true);
							} else {
								// save it
								theDatasetBean = datasetBean;

								// fill the form
								titleTextBox.setText(datasetBean.getTitle());
								descriptionTextarea.setText(datasetBean.getDescription());
								versionTextbox.setText(String.valueOf(datasetBean.getVersion()));
								authorTextbox.setText(datasetBean.getAuthorFullName());
								authorEmailTextbox.setText(datasetBean.getAuthorEmail());
								maintainerTextbox.setText(datasetBean.getMaintainer());
								maintainerEmailTextbox.setText(datasetBean.getMaintainerEmail());

								// Setting visibility, see #28736
								boolean setPublic = datasetBean.getVisibility();
								GWT.log("The visibility is: " + setPublic);
								if (setPublic) {
									visibilityListbox.setSelectedValue(VISIBILITY_PUBLIC_LABEL);
								} else {
									visibilityListbox.setSelectedValue(VISIBILITY_RESTRICTED_LABEL);
								}

								LoaderIcon loader = new LoaderIcon("Retrieving dataset information, please wait...");
								setAlertBlock(null, loader, AlertType.INFO, true);

								// vocabulary list of tags has preemption
								List<String> vocabularyTags = datasetBean.getTagsVocabulary();
								if (vocabularyTags != null) {
									tagsPanel.setVocabulary(vocabularyTags);
//									if (datasetBean.getTags() != null) {
//										for (String tag : datasetBean.getTags()) {
//											tagsPanel.addTagElement(tag);
//										}
//
//									}
								}

								// Settings the tags
								if (datasetBean.getTags() != null) {
									for (String tag : datasetBean.getTags()) {
										tagsPanel.addTagElement(tag);
									}

								}

								// THIS BLOCK is it required? Probably not.

								// retrieve custom fields
								Map<String, List<String>> customFieldsMap = datasetBean.getCustomFields();
								// TODO Check if these tags are ok for the vocabulary
								if (customFieldsMap != null && vocabularyTags == null) {
									// get the keys and put them as tags
									Iterator<Entry<String, List<String>>> iteratorOverCustomField = customFieldsMap
											.entrySet().iterator();

									while (iteratorOverCustomField.hasNext()) {
										Map.Entry<java.lang.String, java.util.List<java.lang.String>> entry = iteratorOverCustomField
												.next();

										List<String> values = entry.getValue();

										for (String value : values) {
											// these are fixed key, variable value custom fields
											CustomFieldEntry toAdd = new CustomFieldEntry(eventBusPublisherWidget,
													entry.getKey(), value, false);
											customFieldEntriesList.add(toAdd);
											customFields.add(toAdd);

											// add as tag
											tagsPanel.addTagElement(entry.getKey());
										}
									}
								}
								// END BLOCK

								// set it as visible anyway
								tagsPanel.setVisible(true);

//								// The resource root is the folder id. Es. ID of the Workspace Root folder.
//								if (datasetBean.getResourceRoot() != null) {
//									resourcesSelectByWEMainPanel = new SelectResourceByWEMainPanel(
//											datasetBean.getResourceRoot());
//								}

								// set organizations
								List<OrganizationBean> organizations = datasetBean.getOrganizationList();

								for (OrganizationBean organization : organizations) {
									organizationsListbox.addItem(organization.getTitle());
									nameTitleOrganizationMap.put(organization.getTitle(), organization.getName());
									break; // expecting only one
								}

								// force the selection of the first one, and retrieve the list of profiles
								organizationsListbox.setSelectedIndex(0);

//								// add change handler to dynamically retrieve the list of profiles
//								organizationsListbox.addChangeHandler(new ChangeHandler() {
//
//									@Override
//									public void onChange(ChangeEvent event) {
//										event.preventDefault();
//										organizationsListboxChangeHandlerBody(operation);
//									}
//								});

								// get the name of the organization from the title
								final String orgName = nameTitleOrganizationMap
										.get(organizationsListbox.getSelectedItemText());

								// Putting the selected Organization into bena
								theDatasetBean.setSelectedOrganization(orgName);

								// try to retrieve the profiles
								loader = new LoaderIcon("Retrieving types, please wait...");
								setAlertBlock(null, loader, AlertType.INFO, true);

								// perform remote request of profiles for the selected organization
								ckanServices.getProfileForUpdate(orgName, datasetBean.getChosenType(),
										datasetBean.getId(), new AsyncCallback<MetadataProfileBeanForUpdate>() {

											@Override
											public void onFailure(Throwable caught) {
												setAlertBlock(caught.getMessage(), null, AlertType.ERROR, true);
											}

											@Override
											public void onSuccess(
													final MetadataProfileBeanForUpdate metadataProfileForUpdate) {
												setAlertBlock("", null, AlertType.ERROR, false);

												GWT.log("MetadataProfileBeanForUpdate: " + metadataProfileForUpdate);

												List<MetaDataProfileBean> profiles = metadataProfileForUpdate
														.getListProfileBean();

												if (profiles == null) {
													setAlertBlock(
															"An unknow error occurred while retrieving types, sorry",
															null, AlertType.ERROR, true);
												} else {

													GWT.log("Profile returned are: " + profiles.size());
													if (!GWT.isProdMode()) {
														for (MetaDataProfileBean profile : profiles) {
															GWT.log("Profile title: " + profile.getTitle() + ", type: "
																	+ profile.getType());
														}
													}

													theDatasetBean.setMetadataList(profiles);
													prepareMetadataList(theDatasetBean, operation);
													addCustomField(metadataProfileForUpdate.getCustomFields());
													organizationsListbox.setEnabled(true);
													metadataTypeListbox.setEnabled(true);

													// try to retrieve the licenses
													LoaderIcon loaderLic = new LoaderIcon(
															"Retrieving types, please wait...");
													setAlertBlock(null, loaderLic, AlertType.INFO, true);

													ckanServices.getLicenses(new AsyncCallback<List<LicenseBean>>() {

														@Override
														public void onFailure(Throwable caught) {
															setAlertBlock(caught.getMessage(), null, AlertType.ERROR,
																	true);
														}

														@Override
														public void onSuccess(List<LicenseBean> licenses) {
															setAlertBlock("", null, AlertType.ERROR, false);

															if (licenses != null && !licenses.isEmpty()) {

																licenseBean = licenses;

																// fill the listbox
																for (int i = 0; i < licenses.size(); i++) {
																	licenseListbox.addItem(licenses.get(i).getTitle());
																}

																// set the url of the license, if any
																showLicenseUrl();

																LoaderIcon loaderLic = new LoaderIcon(
																		"Retrieving groups, please wait...");
																setAlertBlock(null, loaderLic, AlertType.INFO, true);

																// request groups
																ckanServices.getUserGroups(orgName,
																		new AsyncCallback<List<OrganizationBean>>() {

																			@Override
																			public void onSuccess(
																					List<OrganizationBean> groups) {
																				if (groups == null) {
																					setAlertBlock(
																							"Error while retrieving groups",
																							null, AlertType.ERROR,
																							true);
																				} else {
																					if (groups.isEmpty()) {
																						groupsControlGroup
																								.setVisible(false);
																					} else {

																						// add groups
																						for (OrganizationBean group : groups) {
																							groupsListbox.addItem(
																									group.getTitle(),
																									group.getName());
																						}
																						hideGroupsAlreadyInProfile(
																								profiles);
																					}
																					// everything went ok
																					setAlertBlock("", null,
																							AlertType.ERROR, false);
																					continueButton.setEnabled(true);
																					resetButton.setEnabled(true);
																					addResourcesButtonStep1
																							.setEnabled(true);

																					bindManageResources(datasetBean,
																							datasetIdOrName, 1);
																				}
																			}

																			@Override
																			public void onFailure(Throwable caught) {
																				setAlertBlock(caught.getMessage(), null,
																						AlertType.ERROR, true);
																			}
																		});

															} else {
																setAlertBlock("Error while retrieving licenses", null,
																		AlertType.ERROR, true);
															}
														}
													});
												}
											}
										});
							}
						}
					});

				} else {
					setAlertBlock(MISSING_PUBLISH_RIGHTS, null, AlertType.WARNING, true);
				}

			}

			@Override
			public void onFailure(Throwable caught) {
				setAlertBlock(MISSING_PUBLISH_RIGHTS, null, AlertType.ERROR, true);
			}
		});

	}

	private void addCustomField(Map<String, List<String>> customFieldsMap) {
		// TODO Check if these tags are ok for the vocabulary
		if (customFieldsMap != null) {

			// get the keys and put them as tags
			Iterator<Entry<String, List<String>>> iteratorOverCustomField = customFieldsMap.entrySet().iterator();

			while (iteratorOverCustomField.hasNext()) {
				Map.Entry<java.lang.String, java.util.List<java.lang.String>> entry = iteratorOverCustomField.next();

				List<String> values = entry.getValue();

				for (String value : values) {
					// these are fixed key, variable value custom fields
					CustomFieldEntry toAdd = new CustomFieldEntry(eventBusPublisherWidget, entry.getKey(), value,
							false);
					customFieldEntriesList.add(toAdd);
					customFields.add(toAdd);
				}
			}
		}
	}

	/**
	 * Add the items to the listbox and put data into the metadataPanel.
	 *
	 * @param theDatasetBean the received bean
	 * @param operation      the operation
	 */
	private void prepareMetadataList(final DatasetBean receivedBean, OPERATION operation) {

		// the profile should be one
		List<MetaDataProfileBean> profiles = receivedBean.getMetadataList();

		if (profiles != null && !profiles.isEmpty()) {
			for (final MetaDataProfileBean metadataBean : profiles) {

				metadataTypeListbox.addItem(metadataBean.getType());

				// add handler on select
				metadataTypeListbox.addChangeHandler(new ChangeHandler() {

					@Override
					public void onChange(ChangeEvent event) {

						String selectedItemText = metadataTypeListbox.getSelectedItemText();
						metadataFieldsPanel.clear();
						if (selectedItemText.equals(NONE_PROFILE)) {
							metadataFieldsPanel.setVisible(false);
							receivedBean.setChosenType(null);
						} else {
							receivedBean.setChosenType(selectedItemText);
							addFields(selectedItemText, operation);
						}
					}
				});
			}

			// hide elements or show them if needed (groups in profiles cannot be present
			// again in groups listbox)
			if (groupsControlGroup.isVisible()) {
				List<String> groupsToHide = new ArrayList<String>();
				for (MetaDataProfileBean profile : profiles)
					groupsToHide.add(profile.getType().toString());

				SelectElement se = groupsListbox.getElement().cast();

				for (int i = 0; i < groupsListbox.getItemCount(); i++) {
					if (groupsToHide.contains(groupsListbox.getItemText(i))) {
						se.getOptions().getItem(i).getStyle().setProperty("display", "none");
					} else
						se.getOptions().getItem(i).getStyle().setProperty("display", "");
				}
			}

			metadataTypesControlGroup.setVisible(true);
		} else {
			// just hide this listbox
			metadataTypesControlGroup.setVisible(false);
			metadataFieldsPanel.clear();
			listOfMetadataFields.clear();
			receivedBean.setChosenType(null);
		}

		Scheduler.get().scheduleDeferred(new ScheduledCommand() {

			@Override
			public void execute() {
				if (profiles.size() == 1) {
					// showChooseProfileForm(false);
					metadataTypeListbox.setSelectedValue(profiles.get(0).getType());
					// customLegend.setText("Insert Information for: " + profiles.get(0).getType(),
					// true);
					DomEvent.fireNativeEvent(Document.get().createChangeEvent(), metadataTypeListbox);
				}
			}

		});
	}

	/**
	 * Add fields of the selected metadata profile to the widget.
	 *
	 * @param selectedItem the selected item
	 * @param operation    the operation
	 */
	protected void addFields(String selectedItem, OPERATION operation) {

		for (MetaDataProfileBean bean : theDatasetBean.getMetadataList()) {
			if (bean.getType().equals(selectedItem)) {

				// clear old data
				listOfMetadataFields.clear();

				// prepare the data
				List<MetadataFieldWrapper> fields = bean.getMetadataFields();
				List<CategoryWrapper> categories = bean.getCategories();

				GWT.log("There are " + categories.size() + " categories for profile " + bean.getTitle());

				// Adding fields without categories
				if (categories == null || categories.isEmpty()) {
					for (MetadataFieldWrapper field : fields) {

						MetaDataField fieldWidget;
						try {
							fieldWidget = new MetaDataField(field, eventBusPublisherWidget, operation);
							metadataFieldsPanel.add(fieldWidget);
							listOfMetadataFields.add(fieldWidget);
						} catch (Exception e) {
							GWT.log("Unable to build such widget", e);
						}
					}
				} else {

					// create the categories, then parse the fields. Fields do not belonging to a
					// category are put at the end

					// Collecting and showing the fields for categories
					for (CategoryWrapper categoryWrapper : categories) {

						List<MetadataFieldWrapper> fieldsForThisCategory = fields.stream()
								.filter(f -> f.getOwnerCategory() != null
										&& f.getOwnerCategory().getId() == categoryWrapper.getId())
								.collect(Collectors.toList());

						CategoryPanel cp = new CategoryPanel(categoryWrapper.getTitle(),
								categoryWrapper.getDescription());
						fields.removeAll(fieldsForThisCategory);

						for (MetadataFieldWrapper metadataFieldWrapper : fieldsForThisCategory) {

							MetaDataField fieldWidget;
							try {
								fieldWidget = new MetaDataField(metadataFieldWrapper, eventBusPublisherWidget,
										operation);
								cp.addField(fieldWidget);
								listOfMetadataFields.add(fieldWidget);
							} catch (Exception e) {
								GWT.log("Unable to build such widget", e);
							}
						}
						metadataFieldsPanel.add(cp);
					}

					// add the remaining one at the end of the categories
					CategoryPanel extrasCategory = new CategoryPanel("Other", null);
					for (MetadataFieldWrapper field : fields) {

						MetaDataField fieldWidget;
						try {
							fieldWidget = new MetaDataField(field, eventBusPublisherWidget, operation);
							extrasCategory.addField(fieldWidget);
							listOfMetadataFields.add(fieldWidget);
						} catch (Exception e) {
							GWT.log("Unable to build such widget", e);
						}
					}
					metadataFieldsPanel.add(extrasCategory);
				}
				metadataFieldsPanel.setVisible(true);
			}
		}
	}

	/**
	 * Adds the custom field event.
	 *
	 * @param e the e
	 */
	@UiHandler("addCustomFieldButton")
	void addCustomFieldEvent(ClickEvent e) {

		CustomFieldEntry toAdd = new CustomFieldEntry(eventBusPublisherWidget, "", "", true);
		customFieldEntriesList.add(toAdd);
		customFields.add(toAdd);

	}

	/**
	 * On continue button.
	 *
	 * @param e the e
	 */
	@UiHandler("continueButton")
	void onContinueButton(ClickEvent e) {

		// validate data
		final String errorMsg = validateDataOnContinue();

		if (errorMsg != null) {

			alertOnContinue("Please check inserted data [" + errorMsg + "]", AlertType.ERROR);
			return;

		} else {

			// better check for title (only if the dataset was not created.. if it is the
			// case, fields are not frozen)
			if (!titleTextBox.isEnabled())
				actionsAfterOnContinue();
			else {
				actionsAfterOnContinue();
			}
		}
	}

	/**
	 * After onContinue ...
	 */
	private void actionsAfterOnContinue() {

		// check what to do
		if (isWorkspaceRequest) {

			// we need to show the page to handle resources one by one from the workspace
			formFirstStep.setVisible(false);
			boolean hideManageRes = hideManageResources();
			int stepActive = -1;
			if (hideManageRes) {
				stepActive = 3;
			} else {
				stepActive = 2;
			}

			formSecondStep.setVisible(!hideManageRes);
			formThirdStep.setVisible(hideManageRes);
			activeWizardStep(stepActive);

			// add the resources to the container panel
			if (workspaceResourcesContainer.getWidget() == null) {
				workspaceResourcesContainer.getElement().getStyle().setMarginLeft(20, Unit.PX);
				// workspaceResourcesContainer.add(resourcesSelectByWEMainPanel);
			}

		} else {

			// this is not a workspace request
			formFirstStep.setVisible(false);
			formThirdStep.setVisible(true);
			activeWizardStep(2);

		}

		if (metadataTypeListbox.getSelectedItemText().equals(NONE_PROFILE))
			selectedProfile.setText("");
		else
			selectedProfile.setText("Selected Type is " + metadataTypeListbox.getSelectedItemText());

	}

	/**
	 * Active wizard step.
	 *
	 * @param step the step
	 */
	private void activeWizardStep(int step) {
		if (wizCreator != null) {
			wizCreator.activeStep(step);
		}
	}

	/**
	 * On go back button first step.
	 *
	 * @param e the e
	 */
	@UiHandler("goBackButtonFirstStep")
	void onGoBackButtonFirstStep(ClickEvent e) {

		// swap forms
		formFirstStep.setVisible(true);
		activeWizardStep(1);
		formSecondStep.setVisible(false);
		formThirdStep.setVisible(false);

	}

	/**
	 * On go back button.
	 *
	 * @param e the e
	 */
	@UiHandler("goBackButtonSecondStep")
	void onGoBackButton(ClickEvent e) {

		// swap forms
		if (isWorkspaceRequest) {
			boolean hideManageRes = hideManageResources();
			int stepActive = -1;
			if (hideManageRes) {
				stepActive = 1;
			} else {
				stepActive = 2;
			}
			formFirstStep.setVisible(hideManageRes);
			formSecondStep.setVisible(!hideManageRes);
			activeWizardStep(stepActive);
		} else {

			formFirstStep.setVisible(true);
			formSecondStep.setVisible(false);
			activeWizardStep(1);
		}
		formThirdStep.setVisible(false);

	}

	/**
	 * On continue third step.
	 *
	 * @param e the e
	 */
	@UiHandler("continueThirdStep")
	void onContinueThirdStep(ClickEvent e) {

		// swap forms
		formSecondStep.setVisible(false);
		formThirdStep.setVisible(true);
		activeWizardStep(3);

	}

	/**
	 * Creates the dataset event.
	 *
	 * @param e the e
	 */
	@UiHandler("updateButton")
	void updateDatasetEvent(ClickEvent e) {

		String errorMessage = areProfileDataValid();

		if (errorMessage != null) {
			alertOnUpdated("Please check the inserted values and the mandatory fields [" + errorMessage + "]", false,
					AlertType.ERROR, true);
		} else {

			String title = titleTextBox.getValue().trim();
			String description = descriptionTextarea.getText().trim();
			String selectedLicense = licenseListbox.getSelectedItemText();
			String visibility = visibilityListbox.getSelectedItemText();
			long version = Long.valueOf(versionTextbox.getValue().trim());
			String author = authorTextbox.getValue();
			String authorEmail = authorEmailTextbox.getValue();
			String maintainer = maintainerTextbox.getValue().trim();
			String maintainerEmail = maintainerEmailTextbox.getValue().trim();
			String chosenOrganizationTitle = organizationsListbox.getSelectedItemText();
			Set<String> tags = new HashSet<String>(tagsPanel.getTags());

			// we need to retrieve the organization's name from this title
			List<OrganizationBean> orgs = theDatasetBean.getOrganizationList();
			String chosenOrganization = null;
			for (OrganizationBean organizationBean : orgs) {
				if (chosenOrganizationTitle.equals(organizationBean.getTitle())) {
					chosenOrganization = organizationBean.getName();
					break;
				}
			}

			List<OrganizationBean> groups = new ArrayList<OrganizationBean>();
			List<OrganizationBean> groupsToForceCreation = new ArrayList<OrganizationBean>();

			// get groups, if any
			int items = groupsListbox.getItemCount();
			for (int i = 0; i < items; i++) {
				String groupTitle = groupsListbox.getItemText(i);
				String groupName = groupsListbox.getValue(i);
				if (groupsListbox.isItemSelected(i)) {
					groups.add(new OrganizationBean(groupTitle, groupName, false));
				}
			}

			Map<String, List<String>> customFieldsMap = new HashMap<String, List<String>>();

			// prepare custom fields
			for (MetaDataField metaField : listOfMetadataFields) {

				for (MetaDataFieldSkeleton field : metaField.getListOfMetadataFields()) {

					List<String> valuesForField = field.getFieldCurrentValue();
					if (!valuesForField.isEmpty()) {
						String key = field.getFieldNameQualified();
						List<String> valuesForThisField = null;
						if (customFieldsMap.containsKey(key))
							valuesForThisField = customFieldsMap.get(key);
						else
							valuesForThisField = new ArrayList<String>();

						valuesForThisField.addAll(valuesForField);
						customFieldsMap.put(key, valuesForThisField);

						// get also tag/group if it is the case for this field
						List<String> tagsField = field.getTagFromThisField();
						if (tagsField != null)
							tags.addAll(tagsField);

						List<String> groupsTitle = field.getGroupTitleFromThisGroup();
						if (groupsTitle != null) {
							for (String groupTitle : groupsTitle) {
								if (field.isGroupToForce())
									groupsToForceCreation.add(
											new OrganizationBean(groupTitle, groupTitle, false, field.isPropagateUp()));
								else
									groups.add(
											new OrganizationBean(groupTitle, groupTitle, false, field.isPropagateUp()));
							}
						}
					}
				}
			}

			for (CustomFieldEntry customEntry : customFieldEntriesList) {
				String key = customEntry.getKey();
				String value = customEntry.getValue();
				if (value != null && !value.isEmpty()) {
					List<String> valuesForThisField = null;
					if (customFieldsMap.containsKey(key))
						valuesForThisField = customFieldsMap.get(key);
					else
						valuesForThisField = new ArrayList<String>();
					valuesForThisField.add(value);
					customFieldsMap.put(key, valuesForThisField);
				}

			}

			// fill the bean
			theDatasetBean.setAuthorFullName(author);
			theDatasetBean.setAuthorEmail(authorEmail);
			theDatasetBean.setDescription(description);
			theDatasetBean.setLicense(selectedLicense);
			theDatasetBean.setMaintainer(maintainer);
			theDatasetBean.setMaintainerEmail(maintainerEmail);
			theDatasetBean.setVersion(version);
			theDatasetBean.setVisibile(visibility.equals(VISIBILITY_PUBLIC_LABEL));
			theDatasetBean.setTitle(title);
			theDatasetBean.setTags(new ArrayList<String>(tags));
			theDatasetBean.setSelectedOrganization(chosenOrganization);
			theDatasetBean.setGroups(groups);
			theDatasetBean.setGroupsForceCreation(groupsToForceCreation);

//			if (resourcesSelectByWEMainPanel != null) {
//				theDatasetBean.setResourceRoot(resourcesSelectByWEMainPanel.getResourcesToPublish());
//			}

			theDatasetBean.setCustomFields(customFieldsMap);

			// alert
			alertOnUpdated(TRYING_TO_UPDATE_PRODUCT, true, AlertType.INFO, false);

			// invoke the create method
			updateButton.setEnabled(false);
			goBackButtonSecondStep.setEnabled(false);

			ckanServices.updateCKANDataset(theDatasetBean, new AsyncCallback<DatasetBean>() {

				@Override
				public void onSuccess(final DatasetBean createdDatasetBean) {
					CKanMetadataPublisher.printJs("createCKanDataset returned");

					GWT.log("Updated the dataset: " + createdDatasetBean);

					if (createdDatasetBean != null) {

						CKanMetadataPublisher.printJs(createdDatasetBean.toString());

						final String datasetUrl = createdDatasetBean.getSource();

						String successMsg = catalogueModerated != null && catalogueModerated
								? PRODUCT_UPDATED_SUBMITTED_FOR_REVIEW_OK
								: PRODUCT_UPDATED_OK;

						alertOnUpdated(successMsg, false, AlertType.SUCCESS, false);

						try {
							// disable dataset fields
							disableDatasetFields();
						} catch (Exception e) {
							// TODO: handle exception
						}

						// disable reset
						resetButton.setEnabled(false);

						// show the go to dataset button

						goToDatasetButtonPanel.setVisible(true);
						goToDatasetButton.setVisible(true);

						String title = createdDatasetBean.getTitle();
						String link = "(click here) ";
						link += title.length() > 90 ? title.substring(0, 90) + "..." : title;

						goToDatasetButton.setTitle("Go to the item: " + title);

						goToDatasetButton.setText(link);
						goToDatasetButton.addClickHandler(new ClickHandler() {

							@Override
							public void onClick(ClickEvent event) {
								Window.open(datasetUrl, "_blank", "");
								// Window.Location.assign(datasetUrl);
							}
						});

						// set hidden the create button
						updateButton.setVisible(false);

						// if we are in the "general case" we need to show a form for adding resources
						bindManageResources(createdDatasetBean, datasetUrl, 3);

						eventBusPublisherWidget.fireEvent(new ReloadDatasetPageEvent(createdDatasetBean.getId()));

					} else {

						alertOnUpdated(ERROR_PRODUCT_CREATION, false, AlertType.ERROR, true);
					}

				}

				@Override
				public void onFailure(Throwable caught) {
					alertOnUpdated(ERROR_PRODUCT_CREATION + " Error message is : " + caught.getMessage(), false,
							AlertType.ERROR, true);
					CKanMetadataPublisher.printJs(caught.toString());
				}
			});
		}
	}

	private void bindManageResources(final DatasetBean theDatasetBean, final String datasetUrl, int step) {
		GWT.log("bindManageResources on dataset: " + theDatasetBean.getCkanName());

		if (step == 1) {
			// addResourcesButtonStep1.clear();
			addResourcesButtonStep1.setVisible(true);
			addResourcesButtonStep3.setVisible(false);
			addResourcesButtonStep1.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					GWT.log("Clicked addResourcesButtonStep1");

					manageResourcesOnFire(theDatasetBean, datasetUrl);
				}
			});
		}

		if (step == 3) {
			// addResourcesButtonStep3.clear();
			addResourcesButtonStep3.setVisible(true);
			addResourcesButtonStep3.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					GWT.log("Clicked addResourcesButtonStep3");

					manageResourcesOnFire(theDatasetBean, datasetUrl);

				}
			});
		}
	}

	private void manageResourcesOnFire(final DatasetBean theDatasetBean, String datasetUrl) {

		// remove content of the main panel
		createDatasetMainPanel.clear();

		ManageResources manageResources = new ManageResources(eventBusPublisherWidget, theDatasetBean, datasetUrl);

		// add the new content of the main panel
		createDatasetMainPanel.add(manageResources);

		final List<ResourceElementBean> resources = theDatasetBean.getCkanResources();

		if (resources != null && resources.size() > 0) {
			GWT.log("Executing are: " + resources);
			for (ResourceElementBean reb : theDatasetBean.getCkanResources()) {
				manageResources.getEventBus().fireEvent(new AddResourceEvent(reb));
			}
		}
	}

	/**
	 * Prepare the info icons of all core metadata info.
	 */
	private void prepareInfoIcons() {

		// tags
		tagsPanel.prepareIcon(popupOpenedIds);

		// licenses
		InfoIconsLabels.preparePopupPanelAndPopover(InfoIconsLabels.LICENSES_INFO_ID_POPUP,
				InfoIconsLabels.LICENSES_INFO_TEXT, InfoIconsLabels.LICENSES_INFO_CAPTION, infoIconLicenses,
				popoverLicenses, focusPanelLicenses, popupOpenedIds);

		// visibility
		InfoIconsLabels.preparePopupPanelAndPopover(InfoIconsLabels.VISIBILITY_INFO_ID_POPUP,
				InfoIconsLabels.VISIBILITY_INFO_TEXT, InfoIconsLabels.VISIBILITY_INFO_CAPTION, infoIconVisibility,
				popoverVisibility, focusPanelVisibility, popupOpenedIds);

		// author
		InfoIconsLabels.preparePopupPanelAndPopover(InfoIconsLabels.AUTHOR_INFO_ID_POPUP,
				InfoIconsLabels.AUTHOR_INFO_TEXT, InfoIconsLabels.AUTHOR_INFO_CAPTION, infoIconAuthor, popoverAuthor,
				focusPanelAuthor, popupOpenedIds);

		// author's email
		InfoIconsLabels.preparePopupPanelAndPopover(InfoIconsLabels.AUTHOR_EMAIL_INFO_ID_POPUP,
				InfoIconsLabels.AUTHOR_EMAIL_INFO_TEXT, InfoIconsLabels.AUTHOR_EMAIL_INFO_CAPTION, infoIconAuthorEmail,
				popoverAuthorEmail, focusPanelAuthorEmail, popupOpenedIds);

		// maintainer
		InfoIconsLabels.preparePopupPanelAndPopover(InfoIconsLabels.MAINTAINER_INFO_ID_POPUP,
				InfoIconsLabels.MAINTAINER_INFO_TEXT, InfoIconsLabels.MAINTAINER_INFO_CAPTION, infoIconMaintainer,
				popoverMaintainer, focusPanelMaintainer, popupOpenedIds);

		// maintainer's email
		InfoIconsLabels.preparePopupPanelAndPopover(InfoIconsLabels.MAINTAINER_EMAIL_INFO_ID_POPUP,
				InfoIconsLabels.MAINTAINER_EMAIL_INFO_TEXT, InfoIconsLabels.MAINTAINER_EMAIL_INFO_CAPTION,
				infoIconMaintainerEmail, popoverMaintainerEmail, focusPanelMaintainerEmail, popupOpenedIds);

		// profiles (or types)
		InfoIconsLabels.preparePopupPanelAndPopover(InfoIconsLabels.PROFILES_INFO_ID_POPUP,
				InfoIconsLabels.PROFILES_INFO_TEXT, InfoIconsLabels.PROFILES_INFO_CAPTION, infoIconTypes, popoverTypes,
				focusPanelTypes, popupOpenedIds);

		// custom fields
		InfoIconsLabels.preparePopupPanelAndPopover(InfoIconsLabels.CUSTOM_FIELDS_INFO_ID_POPUP,
				InfoIconsLabels.CUSTOM_FIELDS_INFO_TEXT, InfoIconsLabels.CUSTOM_FIELDS_INFO_CAPTION,
				infoIconCustomFields, popoverCustomFields, focusPanelCustomFields, popupOpenedIds);

		// resources field
		InfoIconsLabels.preparePopupPanelAndPopover(InfoIconsLabels.RESOURCES_INFO_ID_POPUP,
				InfoIconsLabels.RESOURCES_INFO_TEXT, InfoIconsLabels.RESOURCES_INFO_CAPTION, infoIconResources,
				popoverResources, focusPanelResources, popupOpenedIds);

		// title
		InfoIconsLabels.preparePopupPanelAndPopover(InfoIconsLabels.TITLE_INFO_ID_POPUP,
				InfoIconsLabels.TITLE_INFO_TEXT, InfoIconsLabels.TITLE_INFO_CAPTION, infoIconTitle, popoverTitle,
				focusPanelTitle, popupOpenedIds);

		// description
		InfoIconsLabels.preparePopupPanelAndPopover(InfoIconsLabels.DESCRIPTION_INFO_ID_POPUP,
				InfoIconsLabels.DESCRIPTION_INFO_TEXT, InfoIconsLabels.DESCRIPTION_INFO_CAPTION, infoIconDescription,
				popoverDescription, focusPanelDescription, popupOpenedIds);

		// groups
		InfoIconsLabels.preparePopupPanelAndPopover(InfoIconsLabels.GROUPS_INFO_ID_POPUP,
				InfoIconsLabels.GROUPS_INFO_TEXT, InfoIconsLabels.GROUPS_INFO_CAPTION, infoIconGroups, popoverGroups,
				focusPanelGroups, popupOpenedIds);
	}

	/**
	 * Test if profile data are valid.
	 *
	 * @return the string
	 */
	private String areProfileDataValid() {

		for (MetaDataField metaField : listOfMetadataFields) {

			for (MetaDataFieldSkeleton field : metaField.getListOfMetadataFields()) {

				field.removeError();

				String error = field.isFieldValueValid();
				if (error != null) {
					field.showError();
					return field.getFieldNameOriginal() + " is not valid. Suggestion: " + error;
				}
			}
		}

		return null;
	}

	/**
	 * On continue show alert box and enable buttons.
	 *
	 * @param text the text
	 * @param type the type
	 */
	private void alertOnContinue(String text, AlertType type) {

		onContinueAlertBlock.setText(text);
		onContinueAlertBlock.setType(type);
		onContinueAlertBlock.setVisible(true);
		continueButton.setEnabled(true);
		resetButton.setEnabled(true);

		// hide after some seconds
		Timer t = new Timer() {

			@Override
			public void run() {

				onContinueAlertBlock.setVisible(false);

			}
		};

		t.schedule(4000);
	}

	/**
	 * On continue show alert box and enable buttons.
	 *
	 * @param text            the text
	 * @param showSpinner     the show spinner
	 * @param type            the type
	 * @param hideAfterAWhile the hide after A while
	 */
	private void alertOnUpdated(String text, boolean showSpinner, AlertType type, boolean hideAfterAWhile) {

		onCreateAlertBlock.clear();

		try {
			onCreateAlertBlock.remove(iconSpinner);
		} catch (Exception e) {
		}

		if (showSpinner) {
			onCreateAlertBlock.add(iconSpinner);
		}

		onCreateAlertBlock.setText(text);
		onCreateAlertBlock.setType(type);
		onCreateAlertBlock.setVisible(true);
		updateButton.setEnabled(true);
		goBackButtonSecondStep.setEnabled(true);

		if (hideAfterAWhile) {
			// hide after some seconds
			Timer t = new Timer() {

				@Override
				public void run() {

					onCreateAlertBlock.setVisible(false);

				}
			};
			t.schedule(15000);
		}
	}

	/**
	 * Validate data.
	 *
	 * @return true on success, false otherwise
	 */
	private String validateDataOnContinue() {

		// remove errors
		productTitleGroup.setType(ControlGroupType.NONE);
		maintainerControlGroup.setType(ControlGroupType.NONE);
		versionControlGroup.setType(ControlGroupType.NONE);
		metadataTypesControlGroup.setType(ControlGroupType.NONE);
		organizationsGroup.setType(ControlGroupType.NONE);
		tagsPanel.setGroupPanelType(ControlGroupType.NONE);

		String title = titleTextBox.getText().trim();
		if (title.isEmpty()) {
			productTitleGroup.setType(ControlGroupType.ERROR);
			return "Missing title";
		}

		if (title.length() < 2) {
			productTitleGroup.setType(ControlGroupType.ERROR);
			return "The field title is too short";
		}

		// better check for the title
		// String[] splittedTitle = title.split(" ");

		/*
		 * No check, see #20828 for (String word : splittedTitle) { String replaced =
		 * word.replaceAll(REGEX_TITLE_PRODUCT_SUBWORD, ""); if(!replaced.equals(word)){
		 * productTitleGroup.setType(ControlGroupType.ERROR); return
		 * "Please note not all characters are allowed for the title"; } }
		 */

		// email reg expression
		String maintainerMail = maintainerEmailTextbox.getText();
		if (!maintainerMail.isEmpty() && !maintainerMail.matches(REGEX_MAIL)) {
			maintainerControlGroup.setType(ControlGroupType.ERROR);
			return "Not valid maintainer email";
		}

		// check if version is a number
		try {
			int number = Integer.valueOf(versionTextbox.getText().trim());
			if (number <= 0)
				throw new Exception();
		} catch (Exception e) {
			versionControlGroup.setType(ControlGroupType.ERROR);
			return "Version must be a natural number greater than zero";
		}

		// check if metadata profile is different from none and its mandatory fields
		// have been fulfilled
		if (checkSelectedMetaDataProfile()) {
			metadataTypesControlGroup.setType(ControlGroupType.ERROR);
			return "You must select a Type different frome none";
		}

		if (organizationsListbox.getSelectedItemText() == null) {
			organizationsGroup.setType(ControlGroupType.ERROR);
			return "You must select an organization in which you want to publish";
		}

		// at least one tag..
		if (tagsPanel.getTags().isEmpty()) {
			tagsPanel.setGroupPanelType(ControlGroupType.ERROR);
			return "Please add at least one meaningful tag for the item";
		}

		return null;
	}

	/**
	 * Checks if a metadata profile has been chosen and its fields have been
	 * fulfilled.
	 *
	 * @return true, if successful
	 */
	private boolean checkSelectedMetaDataProfile() {
		return metadataTypeListbox.getSelectedItemText().equals(NONE_PROFILE)
				&& metadataTypeListbox.getItemCount() != 1;
	}

	/**
	 * Reset form event.
	 *
	 * @param e the e
	 */
	@UiHandler("resetButton")
	void resetFormEvent(ClickEvent e) {

		// reset main fields
		titleTextBox.setText("");
		descriptionTextarea.setText("");
		versionTextbox.setText("");
		maintainerTextbox.setText("");
		maintainerEmailTextbox.setText("");
		tagsPanel.removeTags();

		// unselect all groups
		for (int i = 0; i < groupsListbox.getItemCount(); i++)
			groupsListbox.setItemSelected(i, false);

		// delete custom fields
		for (CustomFieldEntry customField : customFieldEntriesList) {
			customField.removeFromParent();
		}
		customFieldEntriesList.clear();
	}

	/**
	 * Disable dataset editable fields once the dataset has been successfully
	 * created.
	 */
	protected void disableDatasetFields() {

		titleTextBox.setEnabled(false);
		descriptionTextarea.setEnabled(false);
		versionTextbox.setEnabled(false);
		maintainerTextbox.setEnabled(false);
		maintainerEmailTextbox.setEnabled(false);
		visibilityListbox.setEnabled(false);
		tagsPanel.freeze();
		licenseListbox.setEnabled(false);
		organizationsListbox.setEnabled(false);
		addCustomFieldButton.setEnabled(false);
		metadataTypeListbox.setEnabled(false);
		groupsListbox.setEnabled(false);

		for (CustomFieldEntry ce : customFieldEntriesList)
			ce.freeze();

		// disable profile fields
		for (MetaDataField metaField : listOfMetadataFields) {
			for (MetaDataFieldSkeleton field : metaField.getListOfMetadataFields()) {
				field.freeze(true);
			}

		}

//		// freeze table of resources
//		if (resourcesSelectByWEMainPanel != null)
//			resourcesSelectByWEMainPanel.freeze();
	}

	/**
	 * change alert block behavior.
	 *
	 * @param textToShow the text to show
	 * @param type       the type
	 * @param visible    the visible
	 */
	private void setAlertBlock(String textToShow, LoaderIcon loader, AlertType type, boolean visible) {

		alertPanel.clear();

		if (loader != null) {
			alertPanel.add(loader);
		}
		infoBlock.setType(type);
		infoBlock.setVisible(visible);

		if (textToShow != null) {
			alertPanel.add(new HTML(textToShow));
		}

	}

	/**
	 * On selected license change.
	 *
	 * @param c the c
	 */
	@UiHandler("licenseListbox")
	void onSelectedLicenseChange(ChangeEvent c) {

		showLicenseUrl();

	}

	/**
	 * The body of the onSelectedLicenseChange.
	 */
	private void showLicenseUrl() {

		String selectedLicense = licenseListbox.getSelectedItemText();
		int index = -1;
		if ((index = licenseBean.indexOf(new LicenseBean(selectedLicense, null))) >= 0) {
			LicenseBean foundLicense = licenseBean.get(index);
			licenseUrlAnchor.setText(foundLicense.getUrl());
			licenseUrlAnchor.setHref(foundLicense.getUrl());
			licenseUrlAnchor.setVisible(true);
			unavailableUrl.setVisible(false);
		} else {
			licenseUrlAnchor.setVisible(false);
			unavailableUrl.setVisible(true);
		}
	}

	/**
	 * Hide the groups that are already listed in the profiles page.
	 *
	 * @param profiles the profiles
	 */
	private void hideGroupsAlreadyInProfile(List<MetaDataProfileBean> profiles) {

		List<String> groupsToHide = new ArrayList<String>();
		for (MetaDataProfileBean profile : profiles)
			groupsToHide.add(profile.getType());

		SelectElement se = groupsListbox.getElement().cast();

		int hiddenElements = 0;
		for (int i = 0; i < groupsListbox.getItemCount(); i++) {
			if (groupsToHide.contains(groupsListbox.getItemText(i))) {
				se.getOptions().getItem(i).getStyle().setProperty("display", "none");
				hiddenElements++;
			}
		}

		if (hiddenElements == groupsListbox.getItemCount())
			groupsControlGroup.setVisible(false);
		else
			groupsControlGroup.setVisible(true);

	}

	/**
	 * Check if resource(s) are missing.
	 *
	 * @return true, if successful
	 */
	private boolean hideManageResources() {

		return theDatasetBean.getResourceRoot() == null || theDatasetBean.getResourceRoot().isFolder()
				&& (theDatasetBean.getResourceRoot().getChildrenSize() == null
						|| theDatasetBean.getResourceRoot().getChildrenSize() == 0);

	}

	/**
	 * Checks if is catalogue moderated.
	 *
	 * @return the boolean
	 */
	public Boolean isCatalogueModerated() {
		return catalogueModerated;
	}

	public void setCatalogueModerated(Boolean isCatalogueModerated) {
		this.catalogueModerated = isCatalogueModerated;
	}
}
package org.gcube.portlets.widgets.ckandatapublisherwidget.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface ReloadDatasetPageEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 13, 2024
 */
public interface ReloadDatasetPageEventHandler extends EventHandler {

	/**
	 * On added resource.
	 *
	 * @param addResourceEvent the add resource event
	 */
	void onReloadDatasetPage(ReloadDatasetPageEvent addResourceEvent);
}

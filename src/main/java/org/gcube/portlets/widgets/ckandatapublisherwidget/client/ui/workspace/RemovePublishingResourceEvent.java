package org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.workspace;

import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.ResourceElementBean;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class RemovePublishingResourceEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Mar 9, 2021
 */
public class RemovePublishingResourceEvent extends GwtEvent<RemovePublishingResourceEventHandler> {
	
	/** The type. */
	public static Type<RemovePublishingResourceEventHandler> TYPE = new Type<RemovePublishingResourceEventHandler>();

	/** The resource. */
	private ResourceElementBean resource;

	/**
	 * Instantiates a new removes the publishing resource event.
	 *
	 * @param resource the resource
	 */
	public RemovePublishingResourceEvent(ResourceElementBean resource) {
		this.resource = resource;
	}

	/**
	 * Gets the resource.
	 *
	 * @return the resource
	 */
	public ResourceElementBean getResource() {
		return resource;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<RemovePublishingResourceEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(RemovePublishingResourceEventHandler handler) {
		handler.onRemoveResource(this);
	}
}

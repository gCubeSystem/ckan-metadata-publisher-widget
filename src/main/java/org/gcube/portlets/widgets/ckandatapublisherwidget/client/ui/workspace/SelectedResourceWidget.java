package org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.workspace;

import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.ResourceElementBean;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class SelectResourceWidget.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Mar 12, 2021
 */
public class SelectedResourceWidget extends Composite {

	/** The ui binder. */
	private static SelectedResourceWidgetUiBinder uiBinder = GWT.create(SelectedResourceWidgetUiBinder.class);

	/**
	 * The Interface SelectResourceWidgetUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 *         Mar 12, 2021
	 */
	interface SelectedResourceWidgetUiBinder extends UiBinder<Widget, SelectedResourceWidget> {
	}

	/**
	 * Instantiates a new select resource widget.
	 */
	public SelectedResourceWidget() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	/** The button delete. */
	@UiField
	Button buttonDelete;

	/** The button edit. */
	@UiField
	Button buttonEdit;

	/** The field name. */
	@UiField
	Label fieldName;

	@UiField
	Label fieldDescription;

	/** The edit panel. */
	@UiField
	HTMLPanel editPanel;

	@UiField
	FlowPanel resourcePanel;
	
	@UiField
	FlowPanel selectedResourcesPanelTitle;

	private boolean selectetResource = false;

	/** The resource bean. */
	private ResourceElementBean resourceBean;

	/**
	 * Instantiates a new select resource widget.
	 *
	 * @param rb the rb
	 */
	public SelectedResourceWidget(ResourceElementBean rb) {
		initWidget(uiBinder.createAndBindUi(this));
		buttonEdit.setType(ButtonType.LINK);
		buttonDelete.setType(ButtonType.LINK);
		this.resourceBean = rb;
		updateFields();
		addHandlers();

	}

	private void updateFields() {
		this.fieldName.setText(resourceBean.getEditableName());

		if (resourceBean.getDescription() != null && !resourceBean.getDescription().isEmpty()) {
			this.fieldDescription.setVisible(true);
			this.fieldDescription.setText(resourceBean.getDescription());
		} else
			this.fieldDescription.setVisible(false);
	}

	/**
	 * Adds the handlers.
	 */
	private void addHandlers() {

		buttonEdit.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				manageEditOpenResource();

			}
		});

		buttonDelete.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				SelectResourceByWEMainPanel.eventBus.fireEvent(new RemovePublishingResourceEvent(resourceBean));
			}
		});

		MouseDownHandler handler = new MouseDownHandler() {

			@Override
			public void onMouseDown(MouseDownEvent event) {
				manageEditOpenResource();

			}
		};

		selectedResourcesPanelTitle.addDomHandler(handler, MouseDownEvent.getType());
	}

	private void manageEditOpenResource() {

		editPanel.clear();
		if (selectetResource) {
			selectetResource = false;
		} else {
			ResourceInfoForm resourceInformationInfo = new ResourceInfoForm(resourceBean) {
				protected void onUnload() {
					super.onUnload();
					updateFields();
				};
			};
			editPanel.add(resourceInformationInfo);
			selectetResource = true;
		}
	}

	/**
	 * Gets the resource bean.
	 *
	 * @return the resource bean
	 */
	public ResourceElementBean getResourceBean() {
		return resourceBean;
	}

}

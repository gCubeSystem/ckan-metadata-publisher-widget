package org.gcube.portlets.widgets.ckandatapublisherwidget.shared;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * A ckan organization/group (you can check its nature by looking at getIsOrganization();) like bean with name and title
 * @author Costantino Perciante (costantino.perciante@isti.cnr.it)
 */
public class OrganizationBean implements Serializable, IsSerializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8014712837346722595L;
	private String title;
	private String name;
	private boolean isOrganization;
	private boolean propagateUp; // an item linked to this group has to be added on the whole hierarchy chain
	
	public OrganizationBean(){
	}

	public OrganizationBean(String title, String name, boolean isOrganization) {
		this.title = title;
		this.name = name;
		this.isOrganization = isOrganization;
	}
	
	public OrganizationBean(String title, String name, boolean isOrganization, boolean propagateUp) {
		this.title = title;
		this.name = name;
		this.isOrganization = isOrganization;
		this.propagateUp = propagateUp;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isOrganization() {
		return isOrganization;
	}

	public void setOrganization(boolean isOrganization) {
		this.isOrganization = isOrganization;
	}

	public boolean isPropagateUp() {
		return propagateUp;
	}

	public void setPropagateUp(boolean propagateUp) {
		this.propagateUp = propagateUp;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OrganizationBean [title=");
		builder.append(title);
		builder.append(", name=");
		builder.append(name);
		builder.append(", isOrganization=");
		builder.append(isOrganization);
		builder.append(", propagateUp=");
		builder.append(propagateUp);
		builder.append("]");
		return builder.toString();
	}

}

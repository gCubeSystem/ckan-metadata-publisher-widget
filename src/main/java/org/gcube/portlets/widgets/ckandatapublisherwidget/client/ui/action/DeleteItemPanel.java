package org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.action;

import org.gcube.portlets.widgets.ckandatapublisherwidget.client.CKanPublisherServiceAsync;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.ReloadDatasetPageEvent;
import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.DatasetBean;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.utils.LoaderIcon;

import com.github.gwtbootstrap.client.ui.AlertBlock;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class DeleteItemPanel extends Composite {

	private static DeleteItemPanelUiBinder uiBinder = GWT.create(DeleteItemPanelUiBinder.class);

	@UiField
	Button buttonDelete;

	@UiField
	AlertBlock infoBlock;

	@UiField
	HTMLPanel deleteMessage;

	@UiField
	FlowPanel panelConfirm;

	private FlowPanel alertPanel = new FlowPanel();

	// private Icon iconSpinner = new Icon(IconType.SPINNER);

	private HandlerManager eventBusMainApplication;

	private final CKanPublisherServiceAsync ckanServices = CKanPublisherServiceAsync.Util.getInstance();

	private String datasetIdOrName;

	protected static final String MISSING_PUBLISH_RIGHTS = "It seems you are not authorized to delete this item. Either you must be the author item or Catalogue Admininistrator in the organization";

	interface DeleteItemPanelUiBinder extends UiBinder<Widget, DeleteItemPanel> {
	}

	private DatasetBean selectedDataset;

	public DeleteItemPanel(HandlerManager eventBusMainApp, String datasetIdOrName) {
		initWidget(uiBinder.createAndBindUi(this));
		this.datasetIdOrName = datasetIdOrName;
		this.eventBusMainApplication = eventBusMainApp;

		infoBlock.add(alertPanel);

		LoaderIcon loaderIcon = new LoaderIcon("Checking your permissions...");
		setAlertBlock(null, loaderIcon, AlertType.INFO, true);

		ckanServices.isPublisherOwnerOrAdminUser(this.datasetIdOrName, new AsyncCallback<Boolean>() {

			@Override
			public void onFailure(Throwable caught) {
				setAlertBlock(MISSING_PUBLISH_RIGHTS + ". Error: " + caught.getMessage(), null, AlertType.ERROR, true);

			}

			@Override
			public void onSuccess(Boolean result) {
				setAlertBlock("", null, null, false);

				if (result) {
					LoaderIcon loaderIcon = new LoaderIcon("Loading the item....");
					setAlertBlock(null, loaderIcon, AlertType.INFO, true);
					ckanServices.getBasicDatasetBean(datasetIdOrName, new AsyncCallback<DatasetBean>() {

						@Override
						public void onFailure(Throwable caught) {
							setAlertBlock(caught.getMessage(), null, AlertType.ERROR, true);

						}

						@Override
						public void onSuccess(DatasetBean result) {
							setAlertBlock("", null, null, false);
							panelConfirm.setVisible(true);
							selectedDataset = result;
							deleteMessage.add(new HTML("<div><br/><i><b>" + result.getTitle() + "</b></i></div>"));

						}
					});
				} else {
					setAlertBlock(MISSING_PUBLISH_RIGHTS, null, AlertType.WARNING, true);
				}
			}

		});
	}

	@UiHandler("buttonDelete")
	void onClick(ClickEvent e) {

		LoaderIcon loaderIcon = new LoaderIcon("Deleting the item....");
		setAlertBlock(null, loaderIcon, AlertType.INFO, true);
		panelConfirm.setVisible(false);
		ckanServices.deleteItem(selectedDataset, new AsyncCallback<Boolean>() {

			@Override
			public void onFailure(Throwable caught) {
				setAlertBlock("Sorry, an error occurred on deleting the item. Error: " + caught.getMessage(), null,
						AlertType.ERROR, true);

			}

			@Override
			public void onSuccess(Boolean result) {
				if (result) {
					setAlertBlock("Item '" + selectedDataset.getTitle() + "' permanently deleted!", null,
							AlertType.SUCCESS, true);
					eventBusMainApplication.fireEvent(new ReloadDatasetPageEvent(null));
				} else {
					setAlertBlock("Sorry, I cannot delete the item. Refresh and retry or contact the support", null,
							AlertType.INFO, true);
				}

			}
		});
	}

	/**
	 * change alert block behavior.
	 *
	 * @param textToShow the text to show
	 * @param type       the type
	 * @param visible    the visible
	 */
	private void setAlertBlock(String textToShow, LoaderIcon loader, AlertType type, boolean visible) {

		alertPanel.clear();

		if (loader != null)
			alertPanel.add(loader);

		if (type != null)
			infoBlock.setType(type);

		if (textToShow != null)
			alertPanel.add(new HTML(textToShow));

		infoBlock.setVisible(visible);
	}

}

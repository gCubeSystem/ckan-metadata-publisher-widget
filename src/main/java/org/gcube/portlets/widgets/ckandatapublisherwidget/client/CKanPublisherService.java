package org.gcube.portlets.widgets.ckandatapublisherwidget.client;

import java.util.List;

import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.DatasetBean;
import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.MetadataProfileBeanForUpdate;
import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.OrganizationBean;
import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.ResourceElementBean;
import org.gcube.portlets.widgets.mpformbuilder.shared.license.LicenseBean;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * CKAN publisher services.
 * 
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
@RemoteServiceRelativePath("ckanservices")
public interface CKanPublisherService extends RemoteService {

	/**
	 * Retrieve the list of licenses to show to the user.
	 * 
	 * @return a LicenseBean on success, <b>null</b> on error.
	 */
	List<LicenseBean> getLicenses();

	/**
	 * Retrieve the list of profiles for a given organization name .
	 *
	 * @param orgName the org name
	 * @return a List<MetaDataProfileBean> on success, <b>null</b> on error.
	 * @throws Exception the exception
	 */
	List<MetaDataProfileBean> getProfiles(String orgName) throws Exception;

	/**
	 * Retrieve a partially filled bean given a folder id/file id and its owner.
	 *
	 * @param folderIdOrFileId the id of the folder of file to publish
	 * @return @return a DatasetMetadataBean on success, <b>null</b> on error.
	 * @throws Exception the exception
	 */
	DatasetBean buildPartialDatasetBean(String folderIdOrFileId) throws Exception;

	/**
	 * Try to create such dataset starting from the information contained into the
	 * toCreate bean.
	 *
	 * @param toCreate the to create
	 * @return the sent bean filled with the needed information
	 * @throws Exception the exception
	 */
	DatasetBean createCKanDataset(DatasetBean toCreate) throws Exception;

	/**
	 * Add this resource to the dataset whose id is datasetId.
	 *
	 * @param resource  the resource
	 * @param organizationName the organization name
	 * @param datasetId the dataset id
	 * @return the resource element bean
	 * @throws Exception the exception
	 */
	ResourceElementBean addResourceToDataset(ResourceElementBean resource, String organizationName, String datasetId) throws Exception;

	/**
	 * Delete this resource from the dataset with id datasetId.
	 *
	 * @param resource the resource
	 * @return <b>true</b> on success, false otherwise
	 * @throws Exception the exception
	 */
	boolean deleteResourceFromDataset(ResourceElementBean resource) throws Exception;

	/**
	 * Given the title the user wants to give to the new product to create, a check
	 * is performed to understand if a dataset with the proposed title (and so the
	 * id generated at server side) already exists.
	 *
	 * @param title   the title
	 * @param orgName the org name
	 * @return true if it exists, false otherwise
	 * @throws Exception the exception
	 */
	boolean datasetIdAlreadyExists(String title, String orgName) throws Exception;

	/**
	 * Retrieve the list of groups the user can choose to associate this product
	 * with.
	 * 
	 * @param orgName retrieve the groups in the context linked to this name. If
	 *                null, returns the one in the current context.
	 * @return a list of groups' beans
	 */
	List<OrganizationBean> getUserGroups(String orgName);

	/**
	 * The method checks if the user is a publisher or he/she doesn't have the
	 * rights to publish.
	 *
	 * @return true if he/she can publish, false otherwise
	 * @throws Exception the exception
	 */
	Boolean isPublisherUser() throws Exception;

	/**
	 * Get the list of vocabulary tags for this scope.
	 *
	 * @param orgName the org name
	 * @return the tags for organization
	 * @throws Exception the exception
	 */
	List<String> getTagsForOrganization(String orgName) throws Exception;

	/**
	 * Validate a geo json field.
	 *
	 * @param json the json
	 * @return true, if is geo JSON valid
	 * @throws Exception the exception
	 */
	boolean isGeoJSONValid(String json) throws Exception;

	/**
	 * Checks if is owner or admin user.
	 *
	 * @param datasetIdOrName the dataset id or name
	 * @return true, if is owner or admin user
	 * @throws Exception the exception
	 */
	Boolean isPublisherOwnerOrAdminUser(String datasetIdOrName) throws Exception;

	/**
	 * Gets the dataset bean for update.
	 *
	 * @param itemID the item ID
	 * @return the dataset bean for update
	 * @throws Exception the exception
	 */
	DatasetBean getDatasetBeanForUpdate(String itemID) throws Exception;


	/**
	 * Gets the profile for update.
	 *
	 * @param orgName the org name
	 * @param datasetType the dataset type
	 * @param datasedIdOrName the datased id or name
	 * @return the profile for update
	 * @throws Exception the exception
	 */
	MetadataProfileBeanForUpdate getProfileForUpdate(String orgName, String datasetType, String datasedIdOrName) throws Exception;

	/**
	 * Update CKAN dataset.
	 *
	 * @param toUpdate the to create
	 * @return the dataset bean
	 * @throws Exception the exception
	 */
	DatasetBean updateCKANDataset(DatasetBean toUpdate) throws Exception;

	/**
	 * Gets the public link for file item id.
	 *
	 * @param itemId the item id
	 * @param shortenUrl the shorten url
	 * @return the public link for file item id
	 * @throws Exception the exception
	 */
	String getPublicLinkForFileItemId(String itemId, boolean shortenUrl) throws Exception;

	/**
	 * Gets the basic dataset bean.
	 *
	 * @param datasetIdOrName the dataset id or name
	 * @return the basic dataset bean
	 * @throws Exception the exception
	 */
	DatasetBean getBasicDatasetBean(String datasetIdOrName) throws Exception;

	/**
	 * Delete item.
	 *
	 * @param datasetBean the dataset bean
	 * @return true, if successful
	 * @throws Exception the exception
	 */
	boolean deleteItem(DatasetBean datasetBean) throws Exception;
}

package org.gcube.portlets.widgets.ckandatapublisherwidget.client.events;

import org.gcube.portlets.widgets.wsexplorer.shared.Item;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class WorkspaceItemSelectedEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 16, 2024
 */
public class WorkspaceItemSelectedEvent extends GwtEvent<WorkspaceItemSelectedEventHandler> {
	public static Type<WorkspaceItemSelectedEventHandler> TYPE = new Type<WorkspaceItemSelectedEventHandler>();

	private Item item;

	/**
	 * Instantiates a new workspace item selected event.
	 *
	 * @param resource the resource
	 */
	public WorkspaceItemSelectedEvent(Item item) {
		this.item = item;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<WorkspaceItemSelectedEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(WorkspaceItemSelectedEventHandler handler) {
		handler.onSelectedItem(this);
	}

	public Item getItem() {
		return item;
	}
}

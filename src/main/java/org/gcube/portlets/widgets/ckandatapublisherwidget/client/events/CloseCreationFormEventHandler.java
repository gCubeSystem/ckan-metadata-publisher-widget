package org.gcube.portlets.widgets.ckandatapublisherwidget.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * Close event handler interface
 * @author Costantino Perciante at ISTI-CNR 
 * (costantino.perciante@isti.cnr.it)
 *
 */
public interface CloseCreationFormEventHandler extends EventHandler {
	void onClose(CloseCreationFormEvent event);
}

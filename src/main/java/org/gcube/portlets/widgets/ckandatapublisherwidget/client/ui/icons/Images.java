package org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.icons;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface Images extends ClientBundle {
	
	/** The Constant ICONS. */
	public static final Images ICONS = GWT.create(Images.class);
	
	@Source("file.png")
	ImageResource fileIcon();
	
	@Source("folder.png")
	ImageResource folderIcon();
	
	@Source("loading.gif")
	ImageResource loading();
}

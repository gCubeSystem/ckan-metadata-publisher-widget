package org.gcube.portlets.widgets.ckandatapublisherwidget.server;

import org.gcube.datacatalogue.utillibrary.shared.ResourceBean;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanResource;
import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.ResourceElementBean;

/**
 * The Class PublisherCatalogueConveter.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 12, 2024
 */
public class PublisherCatalogueConveter {

	/**
	 * To resource element bean.
	 *
	 * @param ckanResource  the ckan resource
	 * @param orgNameParent the org name parent
	 * @return the resource element bean
	 */
	public static ResourceElementBean toResourceElementBean(CkanResource ckanResource, String orgNameParent) {
		ResourceElementBean reb = new ResourceElementBean();
		reb.setCkanResourceID(ckanResource.getId());
		reb.setName(ckanResource.getName());
		reb.setDescription(ckanResource.getDescription());
		reb.setEditableName(ckanResource.getName());
		reb.setUrl(ckanResource.getUrl());
		reb.setMimeType(ckanResource.getMimetype());
		reb.setOrganizationNameDatasetParent(orgNameParent);
		reb.setCkanDatasetId(ckanResource.getPackageId());
		return reb;
	}

	/**
	 * To resource bean.
	 *
	 * @param ckanResource  the ckan resource
	 * @param orgNameParent the org name parent
	 * @return the resource bean
	 */
	public static ResourceBean toResourceBean(CkanResource ckanResource, String orgNameParent) {
		ResourceBean reb = new ResourceBean();
		reb.setName(ckanResource.getName());
		reb.setDescription(ckanResource.getDescription());
		reb.setId(ckanResource.getId());
		reb.setUrl(ckanResource.getUrl());
		reb.setMimeType(ckanResource.getMimetype());
		reb.setOwner(ckanResource.getOwner());
		reb.setDatasetId(ckanResource.getPackageId());
		return reb;
	}

}

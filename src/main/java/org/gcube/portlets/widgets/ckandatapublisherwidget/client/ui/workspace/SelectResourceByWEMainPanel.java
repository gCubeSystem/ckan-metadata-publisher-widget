package org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.workspace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.ResourceElementBean;
import org.gcube.portlets.widgets.wsexplorer.client.explore.WorkspaceResourcesEnhancedExplorerPanel;
import org.gcube.portlets.widgets.wsexplorer.client.notification.WorkspaceExplorerSelectNotification.WorskpaceExplorerSelectNotificationListener;
import org.gcube.portlets.widgets.wsexplorer.client.view.grid.ItemsTable.DISPLAY_FIELD;
import org.gcube.portlets.widgets.wsexplorer.shared.Item;

import com.github.gwtbootstrap.client.ui.AccordionGroup;
import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Label;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.LabelType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class SelectResourceByWEMainPanel.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Mar 5, 2021
 */
public class SelectResourceByWEMainPanel extends Composite {

	/** The ui binder. */
	private static SelectResourceByWEMainPanelUiBinder uiBinder = GWT.create(SelectResourceByWEMainPanelUiBinder.class);

	private static boolean frozen = false;
	
	private final ResourceElementBean initialBean;

	@UiField
	VerticalPanel wsContainer;
	
	@UiField
	FlowPanel selectResourcesContainer;
	
	@UiField
	Alert showAlert;
	
	@UiField
	Button buttSelectResource;
	
	@UiField
	AccordionGroup buttPickResources;
	
	@UiField
	HTMLPanel containerPickResources;
	
	@UiField
	Label labelNothing;
	
	private Item selectedWEItem;
	
	public final static HandlerManager eventBus = new HandlerManager(null);
	
	private Map<String, SelectedResourceWidget> mapSelectedResources = new HashMap<String, SelectedResourceWidget>();

	/**
	 * The Interface SelectResourceByWEMainPanelUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 *         Mar 5, 2021
	 */
	interface SelectResourceByWEMainPanelUiBinder extends UiBinder<Widget, SelectResourceByWEMainPanel> {
	}

	/**
	 * Instantiates a new select resource by WE main panel.
	 */
	public SelectResourceByWEMainPanel(ResourceElementBean initialBean) {
		initWidget(uiBinder.createAndBindUi(this));
		SelectResourceByWEMainPanel.frozen = false;
		this.initialBean = initialBean;
		this.labelNothing.setType(LabelType.INFO);

		try {
			
			String wsFolderId = null;
			
			//remove this?
			if(initialBean.isFolder()) {
				wsFolderId =  initialBean.getOriginalIdInWorkspace();
			}else {
				//is file
				if(initialBean.getParent()!=null) {
					wsFolderId = initialBean.getParent().getOriginalIdInWorkspace();

				}
			}
			
			//wsFolderId = initialBean.getRootIdInWorkspace()!=null?initialBean.getRootIdInWorkspace():wsFolderId;

			//loads the WE only if the root is not null
			if(wsFolderId!=null) {
				
				DISPLAY_FIELD[] displayFields = new DISPLAY_FIELD[] { DISPLAY_FIELD.ICON, DISPLAY_FIELD.NAME,
						DISPLAY_FIELD.OWNER, DISPLAY_FIELD.CREATION_DATE};
				
				WorkspaceResourcesEnhancedExplorerPanel workspaceExplorerPanel = new WorkspaceResourcesEnhancedExplorerPanel(wsFolderId,false,null, null,false,null,displayFields);
				WorskpaceExplorerSelectNotificationListener wsResourceExplorerListener = new WorskpaceExplorerSelectNotificationListener() {
	
					@Override
					public void onSelectedItem(Item item) {
						GWT.log("Listener Selected Item " + item);
						selectedWEItem = item;
					}
	
					@Override
					public void onFailed(Throwable throwable) {
						// Log.error(throwable.getLocalizedMessage());
						throwable.printStackTrace();
					}
					
	
					@Override
					public void onAborted() {
	
					}
	
					@Override
					public void onNotValidSelection() {
						selectedWEItem = null;
					}
				};
	
				workspaceExplorerPanel.addWorkspaceExplorerSelectNotificationListener(wsResourceExplorerListener);
				wsContainer.add(workspaceExplorerPanel);
			}else {
				containerPickResources.setVisible(false);
			}
			
//			if(rootName!=null) {
//				buttPickResources.setHeading("Add files from "+rootName);
//			}
				

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(!initialBean.isFolder()) {
			addSelectResource(initialBean.getOriginalIdInWorkspace(), initialBean.getName(),initialBean.getFullPath(), initialBean.isFolder());
		}

		addHandlers();
		
	}
	
	private void addHandlers() {
		
		buttSelectResource.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(selectedWEItem!=null && !selectedWEItem.isFolder()) {
					addSelectResource(selectedWEItem.getId(), selectedWEItem.getName(), selectedWEItem.getPath(), selectedWEItem.isFolder());
				}
				
			}
		});
		
		eventBus.addHandler(RemovePublishingResourceEvent.TYPE, new RemovePublishingResourceEventHandler() {
			
			@Override
			public void onRemoveResource(RemovePublishingResourceEvent removeResourceEvent) {
				if(removeResourceEvent.getResource()!=null) {
					removePublishingResource(removeResourceEvent.getResource());
				}
			}

		});
		

	}
	
	
	private void removePublishingResource(ResourceElementBean resource) {
		
		SelectedResourceWidget theRW = mapSelectedResources.get(resource.getOriginalIdInWorkspace());
		if(theRW!=null) {
			selectResourcesContainer.remove(theRW);
			mapSelectedResources.remove(resource.getOriginalIdInWorkspace());
		}
		
		showNothingResourceSelected();
	}

	public void addSelectResource(String wsItemId, String name, String path, boolean isFolder) {

		SelectedResourceWidget selWidg = mapSelectedResources.get(wsItemId);
		if (selWidg != null) {
			showAlertMsg(AlertType.WARNING,"Item '" + name + "' already selected",true);
			return;
		}
		
		ResourceElementBean rb = new ResourceElementBean();
		rb.setOriginalIdInWorkspace(wsItemId);
		rb.setName(name);
		rb.setFullPath(path);
		rb.setEditableName(name);
		rb.setFolder(isFolder);
		selWidg = new SelectedResourceWidget(rb);
		
		mapSelectedResources.put(wsItemId, selWidg);
		selectResourcesContainer.add(selWidg);
		
		showNothingResourceSelected();
		
	}
	
	private void showNothingResourceSelected() {
		if(mapSelectedResources.size()==0) {
			labelNothing.setVisible(true);
		}else
			labelNothing.setVisible(false);
	}
	
	private void showAlertMsg(AlertType type, String txt, boolean autoHide) {
		showAlert.setType(type);
		showAlert.setText(txt);
		showAlert.setVisible(true);
		showAlert.setClose(true);
		if(autoHide) {
			Timer timer = new Timer() {
				
				@Override
				public void run() {
					showAlert.setVisible(false);
					
				}
			};
			
			timer.schedule(5000);
		}
	}



	public void freeze() {
		
		GWT.log("called freeze into selected resources");
		frozen = true;
		selectResourcesContainer.getElement().setAttribute("disabled", "disabled");
		containerPickResources.getElement().setAttribute("disabled", "disabled");
		//buttPickResources.getElement().getStyle().setProperty("pointer-events","none");
		buttSelectResource.setEnabled(false);

	}
	

	/**
	 * Returns the root parent with the children as files to save
	 * @return the resources to save
	 */
	public ResourceElementBean getResourcesToPublish(){

		ResourceElementBean toReturn = new ResourceElementBean();
		List<ResourceElementBean> children = new ArrayList<ResourceElementBean>();
		
		for (String wsItemId : mapSelectedResources.keySet()) {
			SelectedResourceWidget selecWC = mapSelectedResources.get(wsItemId);
			ResourceElementBean theResource = selecWC.getResourceBean();
			theResource.setToBeAdded(true);
			
			if(!theResource.isFolder()){ // be sure ...
				children.add(theResource);
			}
			
		}
		
		toReturn.setToPublish(children);
		GWT.log("resources to publish are: "+toReturn.getToPublish());
		return toReturn;
	}
	

	/**
	 * @return the freezed
	 */
	public static boolean isFroozen() {

		return frozen;
	}


}

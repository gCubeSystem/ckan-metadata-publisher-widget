package org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.resources;

import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.DatasetBean;

import com.github.gwtbootstrap.client.ui.Tab;
import com.github.gwtbootstrap.client.ui.TabPanel;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.github.gwtbootstrap.client.ui.resources.Bootstrap.Tabs;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class ManageResources extends Composite {

	private static ManageResourcesUiBinder uiBinder = GWT.create(ManageResourcesUiBinder.class);

	interface ManageResourcesUiBinder extends UiBinder<Widget, ManageResources> {
	}

	@UiField
	Tab addResources;

	@UiField
	Tab addedResources;

	@UiField
	TabPanel tabPanel;
	
	@UiField
	HTMLPanel manageResourceTitle;
	
	@UiField
	HTML manageResourceSubTitle;

	private AddedResourcesSummary addedResourcesSummary;

	private AddResourceToDataset addResourceForm;

	private HandlerManager eventBus;

	private IconType addedResourcesIcon = IconType.CIRCLE;

	public ManageResources(HandlerManager eventBus, final DatasetBean theDatasetBean, String datasetUrl) {
		initWidget(uiBinder.createAndBindUi(this));
		this.eventBus = eventBus;
		
		manageResourceSubTitle.setText(" "+theDatasetBean.getTitle());

		addResourceForm = new AddResourceToDataset(eventBus, theDatasetBean.getId(), theDatasetBean.getTitle(),
				theDatasetBean.getSelectedOrganization(), datasetUrl);

		addedResourcesSummary = new AddedResourcesSummary(eventBus, addedResources, addedResourcesIcon);

		// tab for the form
		addResources.add(addResourceForm);
		addedResources.add(addedResourcesSummary);
		
		bind();

		tabPanel.setTabPosition(Tabs.ABOVE.name());
		tabPanel.selectTab(0);
		
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			
			@Override
			public void execute() {
				tabPanel.selectTab(0);
			}
		});
	}

	private void bind() {
		
		addedResources.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				addedResources.setIcon(null);
				//addedResources.removeStyle(addedResourcesIcon);
				
			}
		});
	}

	public HandlerManager getEventBus() {
		return eventBus;
	}

}

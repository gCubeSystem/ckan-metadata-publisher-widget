package org.gcube.portlets.widgets.ckandatapublisherwidget.client.events;

import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.ResourceElementBean;

import com.github.gwtbootstrap.client.ui.Button;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Deleted resource event.
 * 
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public class DeleteResourceEvent extends GwtEvent<DeleteResourceEventHandler> {
	public static Type<DeleteResourceEventHandler> TYPE = new Type<DeleteResourceEventHandler>();

	private ResourceElementBean resource;

	private Button deleteButton;

	public DeleteResourceEvent(Button deleteButton, ResourceElementBean resource) {
		this.resource = resource;
		this.deleteButton = deleteButton;
	}

	public ResourceElementBean getResource() {
		return resource;
	}

	@Override
	public Type<DeleteResourceEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(DeleteResourceEventHandler handler) {
		handler.onDeletedResource(this);
	}

	public Button getDeleteButton() {
		return deleteButton;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DeleteResourceEvent [resource=");
		builder.append(resource);
		builder.append(", deleteButton=");
		builder.append(deleteButton);
		builder.append("]");
		return builder.toString();
	}

}

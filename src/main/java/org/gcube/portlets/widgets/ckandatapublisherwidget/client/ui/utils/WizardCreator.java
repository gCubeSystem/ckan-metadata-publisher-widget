package org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.utils;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class WizardCreator.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Mar 15, 2021
 */
public class WizardCreator extends Composite {

	/** The ui binder. */
	private static WizardCreatorUiBinder uiBinder = GWT.create(WizardCreatorUiBinder.class);

	/**
	 * The Interface WizardCreatorUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 * Mar 15, 2021
	 */
	interface WizardCreatorUiBinder extends UiBinder<Widget, WizardCreator> {
	}

	/** The title of steps. */
	private List<String> titleOfSteps;
	
	/** The html steps. */
	private List<HTML> htmlSteps;
	
	/** The wizard container. */
	@UiField
	HTMLPanel theWizardContainer;
	

	/**
	 * Instantiates a new wizard creator.
	 *
	 * @param titleOfSteps the title of steps
	 */
	public WizardCreator(List<String> titleOfSteps) {
		initWidget(uiBinder.createAndBindUi(this));
		this.titleOfSteps = titleOfSteps;
		createSteps();
	}


	/**
	 * Creates the steps.
	 */
	private void createSteps() {
		
		if(titleOfSteps==null)
			return;
		
		int i = 1;
		htmlSteps = new ArrayList<>(titleOfSteps.size());
		for (String step : titleOfSteps) {
			HTML toHML = createStep(i, step);
			htmlSteps.add(toHML);
			theWizardContainer.add(toHML);
			i++;
		}
		
		
		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
			@Override
			public void execute() {
				activeStep(1);
			}
		});
	}
	
	/**
	 * Active step.
	 *
	 * @param stepNumber the step number
	 */
	public void activeStep(int stepNumber) {
		disableAllBadge();
		
		int index = stepNumber-1;
		if(index>=0) {
			HTML html = htmlSteps.get(index);
			html.getElement().addClassName("current");
//			NodeList<Element> spans = html.getElement().getElementsByTagName("span");
//			if(spans!=null && spans.getLength()>0) {
//				Element el = spans.getItem(0);
//				el.addClassName("badge-inverse");
//			}
		}
	}
	
	private void disableAllBadge() {
		for (HTML html : htmlSteps) {
			html.getElement().removeClassName("current");
//			NodeList<Element> spans = html.getElement().getElementsByTagName("span");
//			if(spans!=null && spans.getLength()>0) {
//				Element el = spans.getItem(0);
//				el.removeClassName("badge-inverse");
//			}
		}
		
	}


	/**
	 * Creates the step.
	 *
	 * @param stepNumber the step number
	 * @param stepTitle the step title
	 * @return the string
	 */
	private HTML createStep(int stepNumber, String stepTitle) {
		
		StringBuilder builder = new StringBuilder();
		builder.append("<span class=\"badge-wiz\">");
		builder.append(stepNumber+".");
		builder.append("</span>");
		builder.append(" "+stepTitle);
	
		
		return new HTML(builder.toString());
		
	}
	
	public static class HTMLNoDiv extends HTML{
		
		public static HTML noDiv(String theHtml) {
			HTML html = new HTML() {
				
			};
			
			html.setHTML(theHtml);
			return html;
		}
	}
	
	
	
}

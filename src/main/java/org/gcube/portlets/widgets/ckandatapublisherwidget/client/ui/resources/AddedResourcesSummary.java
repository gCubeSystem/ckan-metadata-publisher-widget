package org.gcube.portlets.widgets.ckandatapublisherwidget.client.ui.resources;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.gcube.portlets.widgets.ckandatapublisherwidget.client.CKanMetadataPublisher;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.AddResourceEvent;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.AddResourceEventHandler;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.DeleteResourceEvent;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.DeleteResourceEventHandler;
import org.gcube.portlets.widgets.ckandatapublisherwidget.client.events.ReloadDatasetPageEvent;
import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.ResourceElementBean;
import org.gcube.portlets.widgets.mpformbuilder.client.ui.utils.LoaderIcon;

import com.github.gwtbootstrap.client.ui.Accordion;
import com.github.gwtbootstrap.client.ui.AccordionGroup;
import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Tab;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.github.gwtbootstrap.client.ui.constants.IconPosition;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class AddedResourcesSummary.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 13, 2024
 */
public class AddedResourcesSummary extends Composite {

	private static AddedResourcesSummaryUiBinder uiBinder = GWT.create(AddedResourcesSummaryUiBinder.class);

	/**
	 * The Interface AddedResourcesSummaryUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Feb 13, 2024
	 */
	interface AddedResourcesSummaryUiBinder extends UiBinder<Widget, AddedResourcesSummary> {
	}

	// Event bus
	private HandlerManager eventBus;

	// list of added resources (beans)
	private List<ResourceElementBean> addedResources;

	private LinkedHashMap<Integer, Accordion> mapAddedResources = new LinkedHashMap<Integer, Accordion>();

	@UiField
	VerticalPanel addResourcesPanel;

	@UiField
	Alert alertMessage;

	@UiField
	Alert alertNoResource;

	private FlowPanel alertPanel = new FlowPanel();

	private Tab tabAddedResources;

	private IconType addedResourcesIcons;

	/**
	 * Instantiates a new added resources summary.
	 *
	 * @param eventBus the event bus
	 */
	public AddedResourcesSummary(HandlerManager eventBus, Tab tabAddedResources, IconType addedResourcesIcons) {
		initWidget(uiBinder.createAndBindUi(this));
		this.tabAddedResources = tabAddedResources;
		this.addedResourcesIcons = addedResourcesIcons;
		this.eventBus = eventBus;

		alertMessage.setType(AlertType.ERROR);
		alertMessage.setClose(true);
		alertMessage.add(alertPanel);

		alertNoResource.setType(AlertType.WARNING);
		alertNoResource.setClose(false);
		alertNoResource.setText("No Resources");
		checkNoResources();
		// bind on add resource event
		bind();
		// init list
		addedResources = new ArrayList<ResourceElementBean>();
	}

	/**
	 * Bind on add/delete resource event.
	 */
	private void bind() {

		// when a new resource is added
		eventBus.addHandler(AddResourceEvent.TYPE, new AddResourceEventHandler() {

			@Override
			public void onAddedResource(AddResourceEvent addResourceEvent) {
				GWT.log("Added resource event: " + addResourceEvent);
				tabAddedResources.setIcon(addedResourcesIcons);
				tabAddedResources.setIconPosition(IconPosition.RIGHT);

				// get the resource
				final ResourceElementBean addedResourceBean = addResourceEvent.getResource();

				// Build an accordion to show resource info
				final Accordion accordion = new Accordion();
				AccordionGroup accordionGroup = new AccordionGroup();
				accordionGroup.setHeading("* " + addedResourceBean.getName());
				accordionGroup.getHeading().addStyleName("accordion-resource-added");
				accordion.add(accordionGroup);

				FlexTable resourceTable = new FlexTable();
				resourceTable.addStyleName("resource-table");

				// add sub-info such as url and description
				// HTML htmlURL = new HTML();
				if (addedResourceBean.getUrl() != null) {
//					htmlURL.setHTML(
//							"URL: <a href=" + addedResourceBean.getUrl() + ">" + addedResourceBean.getUrl() + "</a>");
					resourceTable.setWidget(0, 0, new HTML("URL"));
					resourceTable.setWidget(0, 1, new HTML(
							"<a href=" + addedResourceBean.getUrl() + ">" + addedResourceBean.getUrl() + "</a>"));

				}
				// Paragraph pDescription = new Paragraph();
				// pDescription.setText("Description : " + addedResourceBean.getDescription());

				resourceTable.setWidget(1, 0, new HTML("Description"));
				resourceTable.setWidget(1, 1, new HTML(addedResourceBean.getDescription()));

				// button to delete the resource
				final Button deleteButton = new Button();
				deleteButton.setText("Delete");
				deleteButton.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {

						eventBus.fireEvent(new DeleteResourceEvent(deleteButton, addedResourceBean));

					}
				});

				// fill accordion
				// accordionGroup.add(htmlURL);
				accordionGroup.add(resourceTable);
				accordionGroup.add(deleteButton);

				// add to the list
				addedResources.add(addedResourceBean);

				// add to the panel
				addResourcesPanel.add(accordion);

				mapAddedResources.put(addedResourceBean.getBeanID(), accordion);
				checkNoResources();
			}
		});

		// when the user wants to delete a resource
		eventBus.addHandler(DeleteResourceEvent.TYPE, new DeleteResourceEventHandler() {

			@Override
			public void onDeletedResource(final DeleteResourceEvent deleteResourceEvent) {
				GWT.log("onDeletedResource resource event: " + deleteResourceEvent);

				// to delete
				final ResourceElementBean toDelete = deleteResourceEvent.getResource();

				final Button toDeleteButton = deleteResourceEvent.getDeleteButton();

				toDeleteButton.setEnabled(false);

				LoaderIcon loader = new LoaderIcon("Deleting resource, please wait...");
				setAlertMessage(loader, null, AlertType.INFO, true);

				// find it
				for (int i = 0; i < addedResources.size(); i++) {

					if (addedResources.get(i).equals(toDelete)) {

						// get the associated widget and remove it
						// final Widget widget = addResourcesPanel.getWidget(i);

						final Accordion toDeleteAccordion = mapAddedResources.get(toDelete.getBeanID());

						// remote call to remove it from the dataset
						CKanMetadataPublisher.ckanServices.deleteResourceFromDataset(toDelete,
								new AsyncCallback<Boolean>() {

									@Override
									public void onSuccess(Boolean result) {
										if (result) {
											setAlertMessage(null, "The resource described by '" + toDelete.getName()
													+ "' has been deleted!", AlertType.SUCCESS, true);
											// remove from the list
											addedResources.remove(toDelete);
											addResourcesPanel.remove(toDeleteAccordion);

											// Firing event to reload the dataset page
											eventBus.fireEvent(new ReloadDatasetPageEvent(
													deleteResourceEvent.getResource().getCkanDatasetId()));

											checkNoResources();
										} else {
											setAlertMessage(null, "Sorry, the resource described by '"
													+ toDelete.getName() + "' cannot be deleted", AlertType.SUCCESS,
													true);
										}
									}

									@Override
									public void onFailure(Throwable caught) {
										toDeleteButton.setEnabled(false);
										setAlertMessage(null, caught.getMessage(), AlertType.ERROR, true);
										checkNoResources();
									}
								});

						break;
					}
				}
			}
		});
	}

	private void checkNoResources() {

		if (addedResources != null && addedResources.size() > 0) {
			alertNoResource.setVisible(false);
		} else {
			alertNoResource.setVisible(true);
		}
	}

	/**
	 * Sets the alert message.
	 *
	 * @param loader  the loader
	 * @param message the message
	 * @param type    the type
	 * @param visible the visible
	 */
	private void setAlertMessage(LoaderIcon loader, String message, AlertType type, boolean visible) {

		alertPanel.clear();

		alertMessage.setType(type);
		alertMessage.setVisible(visible);

		if (loader != null) {
			alertPanel.add(loader);
		}

		if (message != null) {
			alertPanel.add(new HTML(message));
		}
	}

	/**
	 * Gets the event bus.
	 *
	 * @return the event bus
	 */
	public HandlerManager getEventBus() {
		return eventBus;
	}
}

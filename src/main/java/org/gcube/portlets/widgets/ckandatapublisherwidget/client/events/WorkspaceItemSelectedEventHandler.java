package org.gcube.portlets.widgets.ckandatapublisherwidget.client.events;

import com.google.gwt.event.shared.EventHandler;

/**
 * The Interface WorkspaceItemSelectedEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 16, 2024
 */
public interface WorkspaceItemSelectedEventHandler extends EventHandler {

	/**
	 * On selected item.
	 *
	 * @param workspaceItemSelectedEvent the workspace item selected event
	 */
	void onSelectedItem(WorkspaceItemSelectedEvent workspaceItemSelectedEvent);
}

package org.gcube.portlets.widgets.ckandatapublisherwidget.client;

import java.util.List;

import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.DatasetBean;
import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.MetadataProfileBeanForUpdate;
import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.OrganizationBean;
import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.ResourceElementBean;
import org.gcube.portlets.widgets.mpformbuilder.shared.license.LicenseBean;
import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * CKAN publisher services RPC.
 * 
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 */
public interface CKanPublisherServiceAsync {

	/**
	 * The Class Util.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Feb 19, 2024
	 */
	public static final class Util {
		private static CKanPublisherServiceAsync instance;

		/**
		 * Gets the single instance of Util.
		 *
		 * @return single instance of Util
		 */
		public static final CKanPublisherServiceAsync getInstance() {
			if (instance == null) {
				instance = (CKanPublisherServiceAsync) GWT.create(CKanPublisherService.class);
			}
			return instance;
		}

		/**
		 * Instantiates a new util.
		 */
		private Util() {
			// Utility class should not be instantiated
		}
	}

	/**
	 * Retrieve the list of licenses to show to the user.
	 *
	 * @param callback the callback
	 * @return a LicenseBean on success, <b>null</b> on error.
	 */
	void getLicenses(AsyncCallback<List<LicenseBean>> callback);

	/**
	 * Retrieve a partially filled bean given a folder id/file id and its owner.
	 *
	 * @param folderIdOrFileId the id of the folder of file to publish
	 * @param callback         the callback
	 * @return @return a DatasetMetadataBean on success, <b>null</b> on error.
	 */
	void buildPartialDatasetBean(String folderIdOrFileId, AsyncCallback<DatasetBean> callback);

	/**
	 * Try to create such dataset starting from the information contained into the
	 * toCreate bean.
	 *
	 * @param toCreate the to create
	 * @param callback the callback
	 * @return the sent bean full filled with the needed information
	 */
	void createCKanDataset(DatasetBean toCreate, AsyncCallback<DatasetBean> callback);

	/**
	 * Add this resource to the dataset whose id is datasetId.
	 *
	 * @param resource         the resource
	 * @param organizationName the organization name
	 * @param datasetId        the dataset id
	 * @param callback         the callback
	 */
	void addResourceToDataset(ResourceElementBean resource, String organizationName, String datasetId,
			AsyncCallback<ResourceElementBean> callback);

	/**
	 * Delete this resource from the dataset with id datasetId.
	 *
	 * @param resource the resource
	 * @param callback the callback
	 * @return <b>true</b> on success, false otherwise
	 */
	void deleteResourceFromDataset(ResourceElementBean resource, AsyncCallback<Boolean> callback);

	/**
	 * Retrieve the list of profiles for a given organization name .
	 *
	 * @param orgName  the org name
	 * @param callback the callback
	 * @return a List<MetaDataProfileBean> on success, <b>null</b> on error.
	 */
	void getProfiles(String orgName, AsyncCallback<List<MetaDataProfileBean>> callback);

	/**
	 * Given the title the user wants to give to the new product to create, a check
	 * is performed to understand if a dataset with the proposed title (and so the
	 * id generated at server side) already exists.
	 *
	 * @param title    the title
	 * @param orgName  the org name
	 * @param callback the callback
	 * @return true if it exists, false otherwise
	 */
	void datasetIdAlreadyExists(String title, String orgName, AsyncCallback<Boolean> callback);

	// /**
	// * Return a tree object representing the whole folder hierarchy
	// * @param folderId
	// * @return ResourceElementBean
	// */
	// void getTreeFolder(String folderId,
	// AsyncCallback<ResourceElementBean> callback);

	/**
	 * Retrieve the list of groups the user can choose to associate this product
	 * with.
	 *
	 * @param orgName  retrieve the groups in the context linked to this name. If
	 *                 null, returns the one in the current context.
	 * @param callback the callback
	 * @return a list of groups' beans
	 */
	void getUserGroups(String orgName, AsyncCallback<List<OrganizationBean>> callback);

	/**
	 * Checks if is publisher user.
	 *
	 * @param callback the callback
	 */
	void isPublisherUser(AsyncCallback<Boolean> callback);

	/**
	 * Get the list of vocabulary tags for this scope.
	 *
	 * @param orgName  the org name
	 * @param callback the callback
	 * @return the tags for organization
	 */
	void getTagsForOrganization(String orgName, AsyncCallback<List<String>> callback);

	/**
	 * Validate a geo json field.
	 *
	 * @param json     the json
	 * @param callback the callback
	 */
	void isGeoJSONValid(String json, AsyncCallback<Boolean> callback);

	/**
	 * Checks if is owner or admin user.
	 *
	 * @param datasetIdOrName the dataset id or name
	 * @param callback        the callback
	 * @return true, if is owner or admin user
	 */
	void isPublisherOwnerOrAdminUser(String datasetIdOrName, AsyncCallback<Boolean> callback);

	/**
	 * Gets the dataset bean.
	 *
	 * @param itemID   the item ID
	 * @param callback the callback
	 * @return the dataset bean
	 */
	void getDatasetBeanForUpdate(String itemID, AsyncCallback<DatasetBean> callback);

	/**
	 * Gets the profile for update.
	 *
	 * @param orgName         the org name
	 * @param datasetType     the dataset type
	 * @param datasedIdOrName the datased id or name
	 * @param callaback       the callaback
	 * @return the profile for update
	 */
	void getProfileForUpdate(String orgName, String datasetType, String datasedIdOrName,
			AsyncCallback<MetadataProfileBeanForUpdate> callaback);

	/**
	 * Update CKAN dataset.
	 *
	 * @param toUpdate  the to update
	 * @param callaback the callaback
	 */
	void updateCKANDataset(DatasetBean toUpdate, AsyncCallback<DatasetBean> callaback);

	/**
	 * Gets the public link for file item id.
	 *
	 * @param itemId     the item id
	 * @param shortenUrl the shorten url
	 * @param callaback  the callaback
	 * @return the public link for file item id
	 */
	void getPublicLinkForFileItemId(String itemId, boolean shortenUrl, AsyncCallback<String> callaback);

	/**
	 * Gets the basic dataset bean.
	 *
	 * @param datasetIdOrName the dataset id or name
	 * @param callaback       the callaback
	 * @return the basic dataset bean
	 */
	void getBasicDatasetBean(String datasetIdOrName, AsyncCallback<DatasetBean> callaback);

	/**
	 * Delete item.
	 *
	 * @param datasetBean the dataset bean
	 * @param callaback   the callaback
	 */
	void deleteItem(DatasetBean datasetBean, AsyncCallback<Boolean> callaback);
}

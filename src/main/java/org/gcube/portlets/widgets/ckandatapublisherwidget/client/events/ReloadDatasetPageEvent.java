package org.gcube.portlets.widgets.ckandatapublisherwidget.client.events;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The Class ReloadDatasetPageEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 13, 2024
 */
public class ReloadDatasetPageEvent extends GwtEvent<ReloadDatasetPageEventHandler> {
	public static Type<ReloadDatasetPageEventHandler> TYPE = new Type<ReloadDatasetPageEventHandler>();

	private String datasetIDorName;

	/**
	 * Instantiates a new reload dataset page event.
	 *
	 * @param resource the resource
	 */
	public ReloadDatasetPageEvent(String datasetIDorName) {
		this.datasetIDorName = datasetIDorName;
	}

	/**
	 * Gets the associated type.
	 *
	 * @return the associated type
	 */
	@Override
	public Type<ReloadDatasetPageEventHandler> getAssociatedType() {
		return TYPE;
	}

	/**
	 * Dispatch.
	 *
	 * @param handler the handler
	 */
	@Override
	protected void dispatch(ReloadDatasetPageEventHandler handler) {
		handler.onReloadDatasetPage(this);
	}

	public String getDatasetIDorName() {
		return datasetIDorName;
	}

}

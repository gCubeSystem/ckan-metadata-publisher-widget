package org.gcube.portlets.widgets.ckandatapublisherwidget.client.events;

import com.google.gwt.event.shared.EventHandler;


/**
 * The Interface DeleteCustomFieldEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Feb 12, 2024
 */
public interface DeleteCustomFieldEventHandler extends EventHandler {
  
  /**
   * On remove entry.
   *
   * @param event the event
   */
  void onRemoveEntry(DeleteCustomFieldEvent event);
}

package org.gcube.portlets.widgets.ckandatapublisherwidget.shared;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.gcube.portlets.widgets.mpformbuilder.shared.metadata.MetaDataProfileBean;

/**
 * The Class MetadataProfileBeanForUpdate.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Feb 8, 2024
 */
public class MetadataProfileBeanForUpdate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -389855531761329707L;
	private List<MetaDataProfileBean> listProfileBean;
	private Map<String, List<String>> customFields;

	/**
	 * Instantiates a new metadata profile bean for update.
	 */
	public MetadataProfileBeanForUpdate() {
		super();
	}

	/**
	 * Instantiates a new metadata profile bean for update.
	 *
	 * @param listProfileBean the list profile bean
	 * @param customFields    the custom fields
	 */
	public MetadataProfileBeanForUpdate(List<MetaDataProfileBean> listProfileBean, Map<String, List<String>> customFields) {
		super();
		this.listProfileBean = listProfileBean;
		this.customFields = customFields;
	}

	/**
	 * Gets the list profile bean.
	 *
	 * @return the list profile bean
	 */
	public List<MetaDataProfileBean> getListProfileBean() {
		return listProfileBean;
	}

	/**
	 * Gets the custom fields.
	 *
	 * @return the custom fields
	 */
	public Map<String, List<String>> getCustomFields() {
		return customFields;
	}

	/**
	 * Sets the list profile bean.
	 *
	 * @param listProfileBean the new list profile bean
	 */
	public void setListProfileBean(List<MetaDataProfileBean> listProfileBean) {
		this.listProfileBean = listProfileBean;
	}

	/**
	 * Sets the custom fields.
	 *
	 * @param customFields the custom fields
	 */
	public void setCustomFields(Map<String, List<String>> customFields) {
		this.customFields = customFields;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MetadataProfileBeanForUpdate [listProfileBean=");
		builder.append(listProfileBean);
		builder.append(", customFields=");
		builder.append(customFields);
		builder.append("]");
		return builder.toString();
	}

}

package org.gcube.portlets.widgets.ckandatapublisherwidget.shared;

import java.io.Serializable;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.view.client.ProvidesKey;

/**
 * A resource element bean. Contains part of the logic used into the TwinColumn
 * widget
 * 
 * @author Costantino Perciante at ISTI-CNR (costantino.perciante@isti.cnr.it)
 * 
 * @author revisited by Francesco Mangiacrapa
 */
public class ResourceElementBean implements Comparable<ResourceElementBean>, Serializable, IsSerializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1230871392599580669L;

	/** The name. */
	private String name;

	/** The editable name. */
	private String editableName;

	/** The to be added. */
	private boolean toBeAdded;

	/** The is folder. */
	private boolean isFolder;

	/** The full path. */
	private String fullPath;

	/** The original id in workspace. */
	private String originalIdInWorkspace;

	private String rootIdInWorkspace;

	/** The mime type. */
	private String mimeType;

	/** The url. */
	private String url;

	/** The description. */
	private String description;

	/** The organization name dataset parent. */
	private String organizationNameDatasetParent; // the organization name in which the parent dataset was created

	/** The parent. */
	private ResourceElementBean parent;

	/** The children size. */
	private Integer childrenSize;

//	/** The to publish. */
	private List<ResourceElementBean> toPublish; // Resources that must be published

	/** The next id. */
	// to generate the GWT identifiers
	private static int nextId = 0;

	/** The identifier GWT. */
	// identifier of this instance
	private int identifierGWT;

	private String ckanResourceID;

	private String ckanDatasetId;

	/**
	 * The key provider that provides the unique ID of a bean.
	 */
	public static final ProvidesKey<ResourceElementBean> KEY_PROVIDER = new ProvidesKey<ResourceElementBean>() {
		@Override
		public Object getKey(ResourceElementBean item) {
			return item == null ? null : item.ckanResourceID;
		}
	};

	/**
	 * Copy constructor.
	 *
	 * @param another the another
	 */
	public ResourceElementBean(ResourceElementBean another) {
		this.name = another.name;
		this.toBeAdded = another.toBeAdded;
		this.fullPath = another.fullPath;
		this.editableName = another.editableName;
		this.originalIdInWorkspace = another.originalIdInWorkspace;
		this.mimeType = another.mimeType;
		this.url = another.url;
		this.description = another.description;
		this.organizationNameDatasetParent = another.organizationNameDatasetParent;
	}

	/**
	 * Default constructor.
	 */
	public ResourceElementBean() {
		super();
		this.identifierGWT = nextId;
		nextId++;
	}

	/**
	 * Instantiates a new resource element bean.
	 *
	 * @param parent       the parent
	 * @param name         the name
	 * @param isFolder     the is folder
	 * @param childrenSize the children size
	 * @param fullPath     the full path
	 */
	public ResourceElementBean(ResourceElementBean parent, String name, boolean isFolder, Integer childrenSize,
			String fullPath) {
		this.identifierGWT = nextId;
		nextId++;
		this.parent = parent;
		this.name = name;
		this.isFolder = isFolder;
		this.childrenSize = childrenSize;
		this.fullPath = fullPath;
	}

	/**
	 * Instantiates a new resource element bean.
	 *
	 * @param name                          the name
	 * @param toBeAdded                     the to be added
	 * @param isFolder                      the is folder
	 * @param parent                        the parent
	 * @param childrenSize                  the children size
	 * @param fullPath                      the full path
	 * @param originalIdInWorkspace         the original id in workspace
	 * @param mimeType                      the mime type
	 * @param url                           the url
	 * @param description                   the description
	 * @param organizationNameDatasetParent the organization name dataset parent
	 */
	public ResourceElementBean(String name, boolean toBeAdded, boolean isFolder, ResourceElementBean parent,
			Integer childrenSize, String fullPath, String originalIdInWorkspace, String mimeType, String url,
			String description, String organizationNameDatasetParent) {
		super();
		this.identifierGWT = nextId;
		nextId++;
		this.name = name;
		this.toBeAdded = toBeAdded;
		this.isFolder = isFolder;
		this.parent = parent;
		this.childrenSize = childrenSize;
		this.fullPath = fullPath;
		this.originalIdInWorkspace = originalIdInWorkspace;
		this.mimeType = mimeType;
		this.url = url;
		this.description = description;
		this.organizationNameDatasetParent = organizationNameDatasetParent;
	}

	/**
	 * Gets the bean ID.
	 *
	 * @return the bean ID
	 */
	public int getBeanID() {
		return identifierGWT;
	}

	/**
	 * Gets the ckan resource ID.
	 *
	 * @return the ckan resource ID
	 */
	public String getCkanResourceID() {
		return ckanResourceID;
	}

	/**
	 * Sets the ckan resource ID.
	 *
	 * @param ckanResourceID the new ckan resource ID
	 */
	public void setCkanResourceID(String ckanResourceID) {
		this.ckanResourceID = ckanResourceID;
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public ResourceElementBean getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	public void setParent(ResourceElementBean parent) {
		this.parent = parent;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Checks if is to be added.
	 *
	 * @return true, if is to be added
	 */
	public boolean isToBeAdded() {
		return toBeAdded;
	}

	/**
	 * Sets the to be added.
	 *
	 * @param toBeAdded the new to be added
	 */
	public void setToBeAdded(boolean toBeAdded) {
		this.toBeAdded = toBeAdded;
	}

	/**
	 * Gets the mime type.
	 *
	 * @return the mime type
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * Sets the mime type.
	 *
	 * @param mimeType the new mime type
	 */
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the organization name dataset parent.
	 *
	 * @return the organization name dataset parent
	 */
	public String getOrganizationNameDatasetParent() {
		return organizationNameDatasetParent;
	}

	/**
	 * Sets the organization name dataset parent.
	 *
	 * @param organizationNameDatasetParent the new organization name dataset parent
	 */
	public void setOrganizationNameDatasetParent(String organizationNameDatasetParent) {
		this.organizationNameDatasetParent = organizationNameDatasetParent;
	}

	/**
	 * Checks if is folder.
	 *
	 * @return true, if is folder
	 */
	public boolean isFolder() {
		return isFolder;
	}

	/**
	 * Sets the folder.
	 *
	 * @param isFolder the new folder
	 */
	public void setFolder(boolean isFolder) {
		this.isFolder = isFolder;
	}

	/**
	 * Gets the children size.
	 *
	 * @return the children size
	 */
	public Integer getChildrenSize() {
		return childrenSize;
	}

	/**
	 * Sets the children size.
	 *
	 * @param size the new children size
	 */
	public void setChildrenSize(Integer size) {
		this.childrenSize = size;
	}

	/**
	 * Gets the full path.
	 *
	 * @return the full path
	 */
	public String getFullPath() {
		return fullPath;
	}

	/**
	 * Sets the full path.
	 *
	 * @param fullPath the new full path
	 */
	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}

	/**
	 * Gets the original id in workspace.
	 *
	 * @return the original id in workspace
	 */
	public String getOriginalIdInWorkspace() {
		return originalIdInWorkspace;
	}

	/**
	 * Sets the original id in workspace.
	 *
	 * @param originalIdInWorkspace the new original id in workspace
	 */
	public void setOriginalIdInWorkspace(String originalIdInWorkspace) {
		this.originalIdInWorkspace = originalIdInWorkspace;
	}

	/**
	 * Gets the editable name.
	 *
	 * @return the editable name
	 */
	public String getEditableName() {
		return editableName;
	}

	/**
	 * Sets the editable name.
	 *
	 * @param newName the new editable name
	 */
	public void setEditableName(String newName) {
		this.editableName = newName;
	}

	/**
	 * Gets the to publish.
	 *
	 * @return the to publish
	 */
	public List<ResourceElementBean> getToPublish() {
		return toPublish;
	}

	/**
	 * Sets the to publish.
	 *
	 * @param toPublish the new to publish
	 */
	public void setToPublish(List<ResourceElementBean> toPublish) {
		this.toPublish = toPublish;
	}

	/**
	 * Equals.
	 *
	 * @param o the o
	 * @return true, if successful
	 */
	@Override
	public boolean equals(Object o) {
		boolean toReturn = false;
		if (o instanceof ResourceElementBean) {
			toReturn = identifierGWT == ((ResourceElementBean) o).identifierGWT;
		}

		return toReturn;
	}

	/**
	 * Compare to.
	 *
	 * @param o the o
	 * @return the int
	 */
	@Override
	public int compareTo(ResourceElementBean o) {
		int toReturn = (o == null || o.fullPath == null) ? -1 : -o.fullPath.compareTo(fullPath);
		return toReturn;
	}

	/**
	 * Gets the root id in workspace.
	 *
	 * @return the root id in workspace
	 */
	public String getRootIdInWorkspace() {
		return rootIdInWorkspace;
	}

	/**
	 * Sets the root id in workspace.
	 *
	 * @param rootIdInWorkspace the new root id in workspace
	 */
	public void setRootIdInWorkspace(String rootIdInWorkspace) {
		this.rootIdInWorkspace = rootIdInWorkspace;
	}

	public void setCkanDatasetId(String packageId) {
		this.ckanDatasetId = packageId;
	}

	public String getCkanDatasetId() {
		return ckanDatasetId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResourceElementBean [name=");
		builder.append(name);
		builder.append(", editableName=");
		builder.append(editableName);
		builder.append(", toBeAdded=");
		builder.append(toBeAdded);
		builder.append(", isFolder=");
		builder.append(isFolder);
		builder.append(", fullPath=");
		builder.append(fullPath);
		builder.append(", originalIdInWorkspace=");
		builder.append(originalIdInWorkspace);
		builder.append(", rootIdInWorkspace=");
		builder.append(rootIdInWorkspace);
		builder.append(", mimeType=");
		builder.append(mimeType);
		builder.append(", url=");
		builder.append(url);
		builder.append(", description=");
		builder.append(description);
		builder.append(", organizationNameDatasetParent=");
		builder.append(organizationNameDatasetParent);
		builder.append(", toPublish=");
		builder.append(toPublish);
		builder.append(", identifierGWT=");
		builder.append(identifierGWT);
		builder.append(", ckanResourceID=");
		builder.append(ckanResourceID);
		builder.append(", ckanDatasetId=");
		builder.append(ckanDatasetId);
		builder.append("]");
		return builder.toString();
	}

}

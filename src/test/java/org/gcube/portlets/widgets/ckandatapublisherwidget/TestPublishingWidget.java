package org.gcube.portlets.widgets.ckandatapublisherwidget;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.storagehubwrapper.server.StorageHubWrapper;
import org.gcube.datacatalogue.utillibrary.server.DataCatalogueFactory;
import org.gcube.datacatalogue.utillibrary.server.DataCatalogueImpl;
import org.gcube.datacatalogue.utillibrary.shared.jackan.model.CkanDataset;
import org.gcube.portlets.widgets.ckandatapublisherwidget.server.CKANPublisherServicesImpl;
import org.gcube.portlets.widgets.ckandatapublisherwidget.server.utils.WorkspaceUtils;
import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.DatasetBean;
import org.gcube.portlets.widgets.ckandatapublisherwidget.shared.OrganizationBean;
import org.junit.Test;
import org.slf4j.LoggerFactory;

/**
 * The Class TestDataCatalogueLib.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy) Jun 1, 2020
 */
public class TestPublishingWidget {

	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(TestPublishingWidget.class);

	private String scope = "/gcube";
	// private String testUser = "costantino_perciante";
	private String testUser = "francesco.mangiacrapa";
	private String authorizationToken = "";

	/**
	 * Before.
	 *
	 * @throws Exception the exception
	 */
	// @Before
	public void before() throws Exception {
	}

	//@Test
	public void getDataset() {

		try {
			scope = "/d4science.research-infrastructures.eu/D4OS/Blue-Cloud2026Project";
			authorizationToken = "8c3ce374-5482-422d-9542-97b1b9360747-843339462"; //Blue-Cloud2026Project
			
			ScopeProvider.instance.set(scope);
			SecurityTokenProvider.instance.set(authorizationToken);
			
			String datasetId = "blue-cloud_2026_-_a_federated_european_fair_and_open_research_ecosystem_for_oceans_seas_coastal_and";

			DataCatalogueFactory factory = DataCatalogueFactory.getFactory();

	
			DataCatalogueImpl utils = factory.getUtilsPerScope(scope);

			CkanDataset dataset = utils.getDataset(datasetId, testUser);
			System.out.println("Dataset: "+dataset);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}

	/**
	 * Factory test.
	 *
	 * @throws Exception the exception
	 */
	// @Test
	public void factoryTest() throws Exception {

		DataCatalogueFactory factory = DataCatalogueFactory.getFactory();

		while (true) {
			factory.getUtilsPerScope(scope);
			Thread.sleep(60 * 1000 * 3);
			factory.getUtilsPerScope(scope);
			break;
		}

		for (int i = 0; i < 5; i++) {
			Thread.sleep(1000);
			factory.getUtilsPerScope(scope);
		}

	}

	// @Test
	public void getDatasetBeanTest() throws Exception {
		ScopeProvider.instance.set(scope);
		String userName = testUser;
		String token = authorizationToken;

		String folderId = "6399daa7-2173-4314-b4f7-2afa24eae8f8";
		DatasetBean bean;
		try {
			bean = new DatasetBean();
			bean.setId(folderId);
			bean.setDescription("This is a fantastic description");
			bean.setVersion(1);
			String onlyAlphanumeric = "test-creation-blablabla".replaceAll("[^A-Za-z0-9]", "");
			bean.setTitle(onlyAlphanumeric + Calendar.getInstance().getTimeInMillis());
			bean.setAuthorName("Francesco");
			bean.setAuthorSurname("Mangiacrapa");
			bean.setAuthorEmail("francesco.mangiacrapa@isti.cnr.it");
			bean.setMaintainer("Francesco Mangiacrapa");
			bean.setMaintainerEmail("francesco.mangiacrapa@isti.cnr.it");

			// UPDATED By Francesco
			String vreName = scope.substring(scope.lastIndexOf("/") + 1, scope.length());
			LOG.debug("In dev mode using the scope: " + scope + " and VRE name: " + vreName);
			bean.setOrganizationList(Arrays.asList(new OrganizationBean(vreName, vreName.toLowerCase(), true)));

			bean.setOwnerIdentifier(userName);

			if (folderId != null && !folderId.isEmpty()) {
				StorageHubWrapper storageHubWrapper = new StorageHubWrapper(scope, token, false, false, true);
				WorkspaceUtils.toWorkspaceResource(folderId, userName, bean, storageHubWrapper.getWorkspace());
			}
		} catch (Exception e) {
			LOG.error("Error while building bean into dev mode", e);
			throw new Exception("Error while retrieving basic information " + e.getMessage());
		}

		LOG.info("Got dataset: " + bean);
	}
	
	//@Test
	public void extrasManage() {
		Map<String, List<String>> extras = new HashMap<String, List<String>>();

		extras.put("ciao", Arrays.asList("ciao value"));
		extras.put(CKANPublisherServicesImpl.NOT_UPGRADABLE_CUSTOM_FIELDS_PREFIXES.get(0), Arrays.asList("rem value 1"));
		extras.put(CKANPublisherServicesImpl.NOT_UPGRADABLE_CUSTOM_FIELDS_PREFIXES.get(1), Arrays.asList("rem value 2"));
		extras.put(CKANPublisherServicesImpl.SYS_TYPE, Arrays.asList("ciao value"));
		extras.put("ultimo value", Arrays.asList("ultimo value"));

		Map<String, List<String>> pop = CKANPublisherServicesImpl.purgeSystemFields(extras);
		System.out.println("POP: " + pop);

		Map<String, List<String>> push = CKANPublisherServicesImpl.getReserverdSystemFields(extras);
		System.out.println("PUSH: " + push);
	}


}
